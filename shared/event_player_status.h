#ifndef SHARED_EVENT_PLAYER_STATUS_H_
#define SHARED_EVENT_PLAYER_STATUS_H_

#include "event.h"

class CEvPlayerStatus : public CProtocolEvent {
private:
	int m_ClientID;
	Vector m_Position;
	bool m_Connected;
	float m_Angle;
	int m_Health;
	bool m_IsProtected;
public:
	CEvPlayerStatus(int client_id, Vector pos, int health, float angle, bool connected, bool protect);
	CEvPlayerStatus(const char* buffer, int length, int start, int & next);
	CEvPlayerStatus();
	~CEvPlayerStatus();
	int CopyToBuffer(char* buffer, int length, int start);
	int GetClientID() { return m_ClientID; }
	Vector GetPosition() { return m_Position; }
	int GetHP() { return m_Health; }
	float GetAngle() { return m_Angle; }
	bool IsConnected() { return m_Connected; }
	bool IsProtected() { return m_IsProtected; }
};

#endif
