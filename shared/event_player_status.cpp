
#include "event_player_status.h"

CEvPlayerStatus::CEvPlayerStatus(int client_id, Vector pos, int health, float angle, bool connected, bool protect) : CProtocolEvent(EH_PL_STATUS) {
	m_ClientID = client_id;
	m_Position = pos;
	m_Health = health;
	m_Angle = angle;
	m_Connected = connected;
	m_IsProtected = protect;
}

CEvPlayerStatus::CEvPlayerStatus(): CEvPlayerStatus(0, Vector(0,0), 0, 0, false, false) {}

CEvPlayerStatus::CEvPlayerStatus(const char* buffer, int length, int start, int& next) : CEvPlayerStatus() {
	if (buffer[start] != EH_PL_STATUS) {
		return;
	}
	next = start + 1;
	next = buffer_to_value(buffer, length, next, &m_ClientID);
	next = buffer_to_value(buffer, length, next, &m_Position);
	next = buffer_to_value(buffer, length, next, &m_Health);
	next = buffer_to_value(buffer, length, next, &m_Angle);
	next = buffer_to_value(buffer, length, next, &m_Connected);
	next = buffer_to_value(buffer, length, next, &m_IsProtected);
}

CEvPlayerStatus::~CEvPlayerStatus() {
}

int CEvPlayerStatus::CopyToBuffer(char* buffer, int length, int start) {

	int next = CProtocolEvent::CopyToBuffer(buffer, length, start);
	if (next < 0) {
		return -1;
	}

	next = value_to_buffer(buffer, length, next, m_ClientID);
	next = value_to_buffer(buffer, length, next, m_Position);
	next = value_to_buffer(buffer, length, next, m_Health);
	next = value_to_buffer(buffer, length, next, m_Angle);
	next = value_to_buffer(buffer, length, next, m_Connected);
	next = value_to_buffer(buffer, length, next, m_IsProtected);

	return next;
}
/* event_player_status.cpp
 *
 *  Created on: Feb 25, 2017
 *      Author: fangzhengwei
*/



