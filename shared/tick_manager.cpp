/*
 * tick_manager.cpp
 *
 *  Created on: Feb 24, 2017
 *      Author: tsfreddie
 */

#include "tick_manager.h"
#include <cstring>

#ifdef Windows
#include <Windows.h>
#include <stdint.h>
int gettimeofday(struct timeval * tp, struct timezone * tzp)
{
	static const uint64_t EPOCH = ((uint64_t)116444736000000000ULL);

	SYSTEMTIME  system_time;
	FILETIME    file_time;
	uint64_t    time;

	GetSystemTime(&system_time);
	SystemTimeToFileTime(&system_time, &file_time);
	time = ((uint64_t)file_time.dwLowDateTime);
	time += ((uint64_t)file_time.dwHighDateTime) << 32;

	tp->tv_sec = (long)((time - EPOCH) / 10000000L);
	tp->tv_usec = (long)(system_time.wMilliseconds * 1000);
	return 0;
}
#endif

CTickManager::CTickManager() {
	gettimeofday(&m_StartClock, NULL);
	m_LastClock = m_StartClock;
}

CTickManager::~CTickManager() {

}

void CTickManager::StartClock() {
	gettimeofday(&m_StartClock, NULL);
	m_LastClock = m_StartClock;
}

double CTickManager::GetTimeSinceStart() {
	struct timeval now;
	gettimeofday(&now, NULL);
	double diff = (now.tv_sec - m_StartClock.tv_sec) * 1000;
	diff += (now.tv_usec - m_StartClock.tv_usec) / (double)1000;
	return diff;
}

double CTickManager::GetDeltaTime() {
	struct timeval now;
	gettimeofday(&now, NULL);
	double diff = (now.tv_sec - m_LastClock.tv_sec) * 1000;
	diff += (now.tv_usec - m_LastClock.tv_usec) / (double)1000;
	m_LastClock = now;
	return diff;
}

int CTickManager::GetTickCount(int tps) {
	double time = GetTimeSinceStart();
	return time / (1000 / (float)tps);
}