#include "network.h"
#include "event.h"

#include <cstring>
#include <iostream>

#ifdef Windows
#define SHUT_RDWR SD_BOTH
#else
#define SOCKET_ERROR -1
#endif

CSocket::CSocket() {
	// Create UDP socket.
	m_Socket = socket(PF_INET, SOCK_DGRAM, 0);
	if (m_Socket < 0)
	{
#ifdef Windows
		int error = WSAGetLastError();
		std::cout << error << std::endl;
		// TODO: Add debug output function
#endif
	}
	m_FromAddrSize = sizeof(m_FromAddr);
    
#ifndef Windows
    m_PollFD.fd = m_Socket;
    m_PollFD.events = POLLIN;
#endif

	std::memset(BeatBuffer, 0, BEATBUFFER_SIZE);
}

CSocket::~CSocket() {
	if (m_Socket >= 0) {
		shutdown(m_Socket, SHUT_RDWR);
#ifdef Windows
		closesocket(m_Socket);
#else
		close(m_Socket);
#endif
	}
}

ReceiveFlag CSocket::ReceiveFrom(char* buffer, int length, int timeout) {
	int poll = 1;
	if (timeout > -1) {
		poll = Select(timeout);
	}

	if (poll < 0)
	{
#ifdef Windows
		int error = WSAGetLastError();
		// TODO: Add debug output function
		std::cout << error << std::endl;
#endif
	}

	if (poll == SOCKET_ERROR)
	{
		return RF_UNDEFINED | RF_ERROR;
	}

	if (poll == 0)
	{
		return RF_UNDEFINED | RF_TIMEOUT;
	}


	m_FromAddrSize = sizeof(m_FromAddr);
	recvfrom(m_Socket, buffer, length, 0, (struct sockaddr*) &m_FromAddr, &m_FromAddrSize);

	return RF_SIMPLE;
}

int CSocket::SendTo(char* buffer, const int length, const struct sockaddr* addr, socklen_t addr_size) {
	return sendto(m_Socket, buffer, length, 0, addr, addr_size);
}

int CSocket::Select(const int timeout_ms) {
#ifndef Windows
    // TODO: figure out why OSX not responding to select()
    // or how to use poll() on Windows.
    return poll(&m_PollFD, 1, timeout_ms);
#else
	struct timeval tv;
    // TODO: calculate seconds from timeout_ms
	tv.tv_sec = 0;
	tv.tv_usec = 1000* timeout_ms;
	FD_ZERO(&m_SelFD);
	FD_SET(m_Socket, &m_SelFD);
	return select(m_Socket, &m_SelFD, NULL, NULL, &tv);
#endif
}

int CSocket::SendBeatBuffer(BeatType beat_type, const struct sockaddr* addr, socklen_t addr_size) {
	std::memset(BeatBuffer, 0, BEATBUFFER_SIZE);
	BeatBuffer[0] = BUFFERTYPE_BEAT;
	BeatBuffer[1] = beat_type;
	BeatBuffer[2] = 0;
	return SendTo(BeatBuffer, BEATBUFFER_SIZE, addr, addr_size);
}


int CSocket::SendBeatBuffer(BeatType beat_type, int arg, const struct sockaddr* addr, socklen_t addr_size) {
	std::memset(BeatBuffer, 0, BEATBUFFER_SIZE);
	BeatBuffer[0] = BUFFERTYPE_BEAT;
	BeatBuffer[1] = beat_type;
	value_to_buffer(BeatBuffer, BEATBUFFER_SIZE, 2, arg);
	BeatBuffer[6] = 0;
	return SendTo(BeatBuffer, BEATBUFFER_SIZE, addr, addr_size);
}


int net_init()
{
	int err = 0;
#ifdef Windows
	WSADATA wsaData;
	err = WSAStartup(MAKEWORD(1, 1), &wsaData);
#endif
	return err;
}

// Static functions
void copy_addr(const struct sockaddr_in* from, socklen_t fromlen, struct sockaddr_in* to, socklen_t& tolen) {
	to->sin_family = from->sin_family;
	to->sin_port = from->sin_port;
	to->sin_addr.s_addr = from->sin_addr.s_addr;
	tolen = fromlen;
}

bool is_same_addr(const struct sockaddr_in * a, const struct sockaddr_in * b) {
	return a->sin_addr.s_addr == b->sin_addr.s_addr && a->sin_port == b->sin_port;
}
