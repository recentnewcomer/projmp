#ifndef SHARED_PLAYER_H_
#define SHARED_PLAYER_H_

#include "vector.h"

class CPlayer {
protected:
    Vector m_Position;
    int m_ClientID;
    bool m_IsActive;
    float m_Angle;
    float m_Speed;
    int m_Status;
    bool m_IsDead;
	float m_GeneralTimer; // For Invicibility and Respawn
    
public:
    CPlayer();
    void Activate(float speed, int cid);
    void Deactivate();
    Vector GetPosition();
    Vector SetPosition(Vector pos);
    float SetAngle(float angle);
    float GetAngle();
    float SetSpeed(float speed);
    float GetSpeed();
	float GetGeneralTimer();
	void SetGeneralTimer(float time);
    int SetStatus(int status);
    int GetStatus();
    bool IsDead();
    bool IsActive() { return m_IsActive; }
    int GetClientID() { return m_ClientID; }
    
    //get hurt
    int TakeDamage();
    //move
    Vector Move(Vector direct);
};

#endif
