/*
 * tick_manager.h
 *
 *  Created on: Feb 24, 2017
 *      Author: tsfreddie
 */
#ifndef SHARED_TICK_MANAGER_H_
#define SHARED_TICK_MANAGER_H_

#ifndef Windows
#include <sys/time.h>
#else
#include <Windows.h>
#endif

class CTickManager {
private:
	struct timeval m_StartClock;
	struct timeval m_LastClock;
public:
	/**
	Manage time and tick info.
	*/
	CTickManager();
	~CTickManager();
	void StartClock();
	/**
	Get the time in milliseconds since the last time called StartClock()
	@return milliseconds
	*/
	double GetTimeSinceStart();
	/**
	Get the time in milliseconds since the last time called GetDeltaTime()
	@return milliseconds
	*/
	double GetDeltaTime();
	/**
	Get the tick count since the last time called StartClock() based on given TICKPERSEC info.
	@param tps Tick per seconds.
	@return milliseconds
	*/
	int GetTickCount(int tps);
};

#endif /* SERVER_TICK_MANAGER_H_ */
