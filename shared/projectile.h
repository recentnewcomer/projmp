#ifndef SHARED_PROJECTILE_H_
#define SHARED_PROJECTILE_H_

#include "vector.h"
#include "player.h"

class CProjectile {
protected:
	Vector m_Position;
	int m_ProjectileID;
	int m_OwnerCID;
	bool m_IsActive;
	float m_Angle;
	float m_Speed;
	float m_Lifespan;

public:
	CProjectile();
	~CProjectile();

	void Activate(Vector init_pos, float angle, float speed, float lifespan, int projectile, int owner);
	void Activate(Vector pos, int projectile, int owner);
	void Deactivate();
	int GetID() { return m_ProjectileID; };
	Vector GetPosition();
	float GetAngle();
	float GetSpeed();
	bool IsActive() { return m_IsActive; }
	int GetOwnerID() { return m_OwnerCID; }
	bool Hit(CPlayer player);

	void Update(float deltaTime);
};

#endif
