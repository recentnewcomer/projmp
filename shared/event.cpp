#include "event.h"
#include <cstring>

CProtocolEvent::CProtocolEvent()
{
	m_EventHeader = EH_UNDEFINED;
}

CProtocolEvent::CProtocolEvent(EventHeader header)
{
	m_EventHeader = header;
}

CProtocolEvent::~CProtocolEvent() {
}

int CProtocolEvent::CopyToBuffer(char * buffer, int length, int start)
{
	if (start < length - 1) {
		std::memcpy(buffer+start, &m_EventHeader, sizeof m_EventHeader);
		return start + sizeof m_EventHeader;
	}
	return -1;
}

// Static functions
// Serialization
int value_to_buffer(char * buffer, int length, int start, std::string value)
{
	// TODO: implement string copy
	// [L][STRING_ITSELF]
	// L = string length (less than 254)
	// NOTE: string might be need its own packet to send the whole string [might need another protocol for string]

	// Do nothing
	return start;
}

int value_to_buffer(char * buffer, int length, int start, const void * value, int value_length)
{
	// TODO: implement string copy
	// [L][MEM_CONTENT]
	// L = buffer length (less than 254)
	// same as above

	// Do nothing
	return start;
}

int buffer_to_value(const char* buffer, int length, int start, std::string& value)
{
	// TODO: see above

	// Do nothing
	return start;
}

int buffer_to_value(const char * buffer, int length, int start, void * value, int value_length)
{
	// TODO: see above

	// Do nothing
	return start;
}
