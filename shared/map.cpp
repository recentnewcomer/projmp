/*
 * map.cpp
 *
 *  Created on: Mar 9, 2017
 *      Author: tsfreddie
 */
#include "map.h"
#include <cstring>

CMap::CMap(int size) {
    m_Size = size;
    m_Tiles = new int* [size];
    for (int i = 0; i < size; i++){
        m_Tiles[i] = new int[size];
        for (int j = 0; j < size; j++){
            m_Tiles[i][j] = 0;
        }
    }
    
    for (int i = 0; i < size; i++ ){
        m_Tiles[i][0] = -1;
        m_Tiles[i][size-1] = -1;
        m_Tiles[0][i] = -1;
        m_Tiles[size-1][i] = -1;
    }
    
    
    
    

}

CMap::~CMap() {
	// TODO Auto-generated destructor stub
    for (int i = 0; i < m_Size; i++) {
        delete []m_Tiles[i];
    }
    delete []m_Tiles;
    m_Tiles = NULL;
    
}

