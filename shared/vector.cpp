/*
 * vector.cpp
 *
 *  Created on: Feb 20, 2017
 *      Author: fangzhengwei
 */

#include "vector.h"
#include <math.h>
Vector::Vector() {
	this->x = 0.0;
	this->y = 0.0;
}

Vector::Vector(float x, float y) {
	this->x = x;
	this->y = y;
}

Vector::~Vector() {

}

Vector Vector::operator+(const Vector& other) const{
	Vector vector(this->x+other.x, this->y+other.y);
	return vector;
}
Vector Vector::operator-(const Vector& other) const{
	Vector vector(this->x-other.x, this->y-other.y);
	return vector;
}
Vector Vector::operator*(float other) const{
	Vector vector(this->x*other, this->y*other);
	return vector;
}
Vector Vector::operator/(float other) const{
	Vector vector(this->x/other, this->y/other);
	return vector;
}
float Vector::getLength(){
	float length = sqrt(pow(this->x,2.0)+pow(this->y,2.0));
	return length;
}
Vector Vector::normalized(){
	Vector vector;
	float length = this->getLength();
	if(length != 0){
		vector.x = (this->x)/length,
		vector.y = (this->y)/length;
	}
	return vector;
}

void Vector::normalize(){
	float length = this->getLength();
	if(length != 0){
		this->x= (this->x)/length;
		this->y = (this->y)/length;
	}

}

