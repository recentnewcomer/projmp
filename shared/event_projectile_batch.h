/*
 * event_projectile_batch.h
 *
 *  Created on: Mar 15, 2017
 *      Author: tsfreddie
 */

#ifndef SHARED_EVENT_POINT_BATCH_H_
#define SHARED_EVENT_POINT_BATCH_H_

#include "event.h"
#include "projectile.h"


class CEvProjectileBatch : public CProtocolEvent {
private:
	CProjectile* m_BatchingObjects;
	CProjectile* m_DeserializedObjects;
	int m_ObjectCount;
public:
	CEvProjectileBatch(CProjectile* objects, char type, int object_count);
	CEvProjectileBatch(const char* buffer, int length, int start, int & next);
	CEvProjectileBatch();
	int GetObjectCount() { return m_ObjectCount; }
	CProjectile* GetObjects() { return m_DeserializedObjects; }
	virtual ~CEvProjectileBatch();
	int CopyToBuffer(char* buffer, int length, int start);
};

#endif /* SHARED_EVENT_POINT_BATCH_H_ */
