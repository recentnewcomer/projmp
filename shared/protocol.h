#ifndef SHARED_PROTOCOL_H_
#define SHARED_PROTOCOL_H_

#include <string>
#include "event.h"

class CProtocol {
private:
	CProtocolEvent** m_apEventList;
	unsigned short m_EventCount;
	unsigned short m_MaxEvent;
public:
	/**
	Serialization protocol.
	@param max_event The max number of events that need to be serialized.
	*/
	CProtocol(unsigned short max_event);

	/**
	Deserialization protocol.
	@param buffer The buffer that need to be deserialized.
	@param length Buffer length.
	*/
	CProtocol(const char* buffer, int length);
	~CProtocol();

	/**
	Add a event into this serializer.
	@param ev Any ProtocolEvent.
	*/
	void AddEvent(CProtocolEvent* ev);

	/**
	Clear the serializer for another batch of events.
	*/
	void ClearProtocol();

	/**
	Serialize all events into a buffer.
	@param buffer The buffer all the data goes into.
	@param length Buffer length.
	*/
	int ToByteBuffer(char* buffer, int length);

	/**
	Get an event pointer.
	@param index The index of event.
	*/
	CProtocolEvent* GetEvent(int index);

	/**
	Get event count.
	[Will convert unsigned short to int for compatibility]
	*/
	int GetEventCount();
};

#endif /* SHARED_PROTOCOL_H_ */
