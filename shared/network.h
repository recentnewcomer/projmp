#ifndef SHARED_NETWORK_H_
#define SHARED_NETWORK_H_

#define BEATBUFFER_SIZE 8

#define BUFFERTYPE_BEAT -2
#define BUFFERTYPE_DATA -1

#define SHARED_BUFFER_SIZE 2048

#ifdef Windows
#include <WinSock2.h>
#include <ws2tcpip.h>
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <poll.h>
#endif

#ifdef Cygwin
#include <sys/select.h>
#endif

typedef enum : char{
	BT_EMPTY = 0,
	BT_HANDSHAKE,
	BT_FAREWELL,
	BT_ACCEPTED,
	BT_REJECTED,
} BeatType;

typedef enum : int {
	RF_UNDEFINED = 0,
	// TYPE
	RF_SIMPLE = 1,
	RF_HANDSHAKE = 2,
	RF_DATA = 4,
	RF_FAREWELL = 8,
	// ACTION
	RF_DROPED = 16,
	// REASON
	RF_FULL = 32,
	RF_PARSING = 64,
	RF_DENIED = 128,
	RF_TIMEOUT = 256,
	RF_ERROR = 512,
} ReceiveFlag;

inline ReceiveFlag operator|(ReceiveFlag a, ReceiveFlag b) { return static_cast<ReceiveFlag>(static_cast<int>(a) | static_cast<int>(b)); }

class CSocket {
private:
	int m_Socket;
	int Select(const int timeout_ms);
	struct sockaddr_in m_FromAddr;
	socklen_t m_FromAddrSize;
#ifndef Windows
    struct pollfd m_PollFD;
#else
    fd_set m_SelFD;
#endif
	char BeatBuffer[BEATBUFFER_SIZE]; // For Handshaking Farewell or Heartbeat
protected:
	/**
	Create a socket.
	*/
	CSocket();
	virtual ~CSocket();

	/**
	Simple warpper for select and recvfrom, will copy any received data to a buffer.
	@param buffer The buffer that data goes to.
	@param length Max buffer size.
	@param timeout blocking time before return a timeout flag. -1 for blocking mode.
	@return	ReceiveFlag, see "network.h"
	*/
	ReceiveFlag ReceiveFrom(char* buffer, int length, int timeout);

	/**
	Simple warpper for sendto, will send the buffer to a specific sockaddr.
	@param buffer The buffer that data come from.
	@param length Buffer size.
	@param addr Target address info
	@param addr_size Target address size
	@return the error code of the original sendto function.
	*/
	int SendTo(char* buffer, int length, const struct sockaddr* addr, socklen_t addr_size);

	/**
	Return the owner address of the latest received data.
	@return a sockaddr_in type address.
	*/
	struct sockaddr_in* GetFromAddr() { return &m_FromAddr; }

	/**
	Return the size of owner address of the latest received data.
	@return a socklen_t type.
	*/
	socklen_t GetFromAddrSize() { return m_FromAddrSize; }

	/**
	Return the socket file descriptor
	@return socket fd as an integer
	*/
	int GetSocket() { return m_Socket; }

	int SendBeatBuffer(BeatType beat_type, const struct sockaddr* addr, socklen_t addr_size);
	int SendBeatBuffer(BeatType beat_type, int arg, const struct sockaddr* addr, socklen_t addr_size);
};

/**
Needed for winsock2.
Do nothing in Linux/Unix environment.
@return winsock2 error number.
*/
int net_init();

/**
Copy address info from one "struct sockaddr_in" to another.
@param from the source struct sockaddr_in
@param fromlen socklen of the source address
@param to the destination struct sockaddr_in
@param tolen socklen of the destination address
*/
void copy_addr(const struct sockaddr_in* from, socklen_t fromlen, struct sockaddr_in* to, socklen_t& tolen);


/**
Compare two sockaddr.
@return true if they are the same.
*/
bool is_same_addr(const struct sockaddr_in* a, const struct sockaddr_in* b);

#endif /* SHARED_NETWORK_H_ */
