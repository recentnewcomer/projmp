#include "player.h"

CPlayer::CPlayer(){
    m_Position = Vector(0,0);
    m_Angle = 0;
    m_Speed = 0;
    m_Status = 3;
    m_IsDead = false;
    m_IsActive = false;
    m_ClientID = -1;
	m_GeneralTimer = 0;
}

void CPlayer::Activate(float speed, int cid) {
	m_ClientID = cid;
	m_Speed = speed;
	m_IsActive = true;
	m_IsDead = false;
	m_Status = 3;
}

void CPlayer::Deactivate() {
	m_IsActive = false;
}

Vector CPlayer::GetPosition(){
    
    return m_Position;
}

float CPlayer::SetAngle(float New_angle){
    if (m_IsDead == 1){
        return m_Angle;
    }

    m_Angle = New_angle;
    return m_Angle;
    
}

float CPlayer::GetAngle(){
    return m_Angle;
}

float CPlayer::SetSpeed(float speed){
    m_Speed = speed;
    return m_Speed;
    
}

int CPlayer::SetStatus(int status) {
	if (status <= 0)
	{
		m_Status = 0;
		m_IsDead = true;
		return 0;
	}
	m_Status = status;
	m_IsDead = false;
	return m_Status;
}

float CPlayer::GetSpeed(){
    return m_Speed;
}

float CPlayer::GetGeneralTimer()
{
	return m_GeneralTimer;
}

void CPlayer::SetGeneralTimer(float time)
{
	m_GeneralTimer = time;
}

int CPlayer::GetStatus(){
    return m_Status;
}

bool CPlayer::IsDead(){
    return m_IsDead;
}

int CPlayer::TakeDamage(){
    if (m_IsDead)
    	return 0;

    m_Status--;
        
    if (m_Status <= 0) {
    	m_Status = 0;
    	m_IsDead = true;
    }

    return 1;
    
}
//move
Vector CPlayer::Move(Vector direct){
    if (m_IsDead){
        return m_Position;
    }
    m_Position = m_Position + direct;
    return m_Position;
}

Vector CPlayer::SetPosition(Vector pos) {
	m_Position = pos;
	return pos;
}
