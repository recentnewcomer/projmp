#include "projectile.h"
#include <math.h>
#include <stdio.h>


CProjectile::CProjectile(){
    m_Position = Vector(0,0);
    m_ProjectileID = 0;
    m_OwnerCID = 0;
    m_IsActive = false;
    m_Angle = 0.0;
    m_Speed = 0.0;
    m_Lifespan = 0.0;
}

CProjectile::~CProjectile() {}

void CProjectile::Activate(Vector init_pos, float angle, float speed, float lifespan, int projectile, int owner){
    m_Position = init_pos;
    m_Angle = angle;
    m_Speed = speed;
    m_Lifespan = lifespan;
    m_ProjectileID = projectile;
    m_OwnerCID = owner;
    m_IsActive = true;
}

void CProjectile::Activate(Vector init_pos, int projectile, int owner) {
    m_Position = init_pos;
    m_Angle = 0;
    m_Speed = 0;
    m_Lifespan = 0;
    m_ProjectileID = projectile;
    m_OwnerCID = owner;
    m_IsActive = true;
}

void CProjectile::Deactivate(){
    m_IsActive = 0;
}
Vector CProjectile::GetPosition(){
    return m_Position;
    
}
float CProjectile::GetAngle(){
    return m_Angle;
}
float CProjectile::GetSpeed(){
    return m_Speed;
}

void CProjectile::Update(float deltaTime){
	m_Lifespan -= deltaTime;
	if (m_Lifespan < 0) {
		Deactivate();
		return;
	}
    float distance = m_Speed * deltaTime;
    float rad = (m_Angle+90)*0.0174533;
    float x_changes = cos(rad) * distance;
    float y_changes = sin(rad) * distance;
    m_Position = m_Position + Vector(x_changes,y_changes);
}

bool CProjectile::Hit(CPlayer player)
{
	Vector pos = player.GetPosition();
	Vector diff = m_Position - pos;

	if (diff.getLength() < 1.5)
		return true;
	return false;
}




