#ifndef SHARED_EVENT_H_
#define SHARED_EVENT_H_

#include <stddef.h>
#include <cstring>
#include <string>
#include "vector.h"

typedef enum : char{
	EH_TEST,
	EH_PL_STATUS,
	EH_PL_INPUT,
	EH_OBJ_BATCH,
	EH_UNDEFINED,
} EventHeader;

// TODO: Automated event object to combine multiple events into one header file.
// TODO: Might need to use struct to build event for ease of use.

class CProtocolEvent
{
private:
	EventHeader m_EventHeader;
public:
	/**
	Network event, contains data that need to be send or already received
	*/
	CProtocolEvent();

	/**
	Network event, contains data that need to be send or already received
	@param header The event header.
	*/
	CProtocolEvent(EventHeader header);

	/**
	@return Event header
	*/
	EventHeader GetEventHeader() { return m_EventHeader; }

	virtual ~CProtocolEvent();

	/**
	Network event, contains data that need to be send or already received
	@param header The event header.
	@param event_size the size of the event.
	@return the end position of the buffer
	*/
	virtual int CopyToBuffer(char* buffer, int length, int start);
};

// Serialization - Value to Buffer

/**
Copy a value to buffer
@param buffer The buffer that data goes to.
@param length The max buffer size.
@param start The starting position that data will be filled within the buffer.
@param value The value that need to be copied.
@return the end position of the buffer
*/
template <typename T>
int value_to_buffer(char * buffer, int length, int start, T value)
{
	if (start + (int)sizeof(T) >= length) {
		return -1;
	}

	std::memcpy(buffer + start, &value, sizeof(T));
	return start + sizeof(T);
}

/**
Copy a value to buffer
@param buffer The buffer that data goes to.
@param length The max buffer size.
@param start The starting position that data will be filled within the buffer.
@param value The value that need to be copied.
@return the end position of the buffer
*/
int value_to_buffer(char* buffer, int length, int start, std::string value);

/**
Copy a byte array to buffer
@param buffer The buffer that data goes to.
@param length The max buffer size.
@param start The starting position that data will be filled within the buffer.
@param value The pointer of the byte array.
@param value_length The length of the data.
@return the end position of the buffer
*/
int value_to_buffer(char* buffer, int length, int start, const void* value, int value_length);

// Deserialization - Buffer to Value

/**
Extract a value from buffer
@param buffer The buffer that data comes from.
@param length The buffer size.
@param start The starting position that data will be extracted from the buffer.
@param value The value that need to be assigned
@return the end position of the buffer
*/
template <typename T>
int buffer_to_value(const char* buffer, int length, int start, T * value)
{
	if (start + (int)sizeof(T) >= length) {
		return -1;
	}
	std::memcpy(value, buffer + start, sizeof(T));
	return start + sizeof(T);;
}

/**
Extract a value from buffer
@param buffer The buffer that data comes from.
@param length The buffer size.
@param start The starting position that data will be extracted from the buffer.
@param value The value that need to be assigned
@return the end position of the buffer
*/
int buffer_to_value(const char* buffer, int length, int start, std::string& value);

/**
Extract a byte array from buffer
@param buffer The buffer that data comes from.
@param length The buffer size.
@param start The starting position that data will be extracted from the buffer.
@param value The byte array that need to be assigned
@param value_length The max array size.
@return the end position of the buffer
*/
int buffer_to_value(const char* buffer, int length, int start, void* value, int value_length);

#endif
