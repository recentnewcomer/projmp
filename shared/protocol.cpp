#include "protocol.h"
#include "network.h"
#include "events.h"
#include <cstring>

CProtocol::CProtocol(unsigned short max_event) {
	m_apEventList = new CProtocolEvent*[max_event];
	m_EventCount = 0;
	m_MaxEvent = max_event;
}

CProtocol::CProtocol(const char * buffer, int length)
{
	m_apEventList = NULL;
	m_EventCount = 0;
	m_MaxEvent = 0;

	if (length <= 0)
		return;

	if (buffer[0] != BUFFERTYPE_DATA)
		return;

	
	int next = buffer_to_value(buffer, length, 1, &m_MaxEvent);

	if (m_MaxEvent == 0)
		return;

	m_apEventList = new CProtocolEvent*[m_MaxEvent];

	while (next != -1 && next < length && m_EventCount < m_MaxEvent) {
		EventHeader header = (EventHeader)buffer[next];
		switch (header) {
		case EH_PL_INPUT:
			AddEvent(new CEvPlayerInput(buffer, length, next, next));
			break;
		case EH_PL_STATUS:
			AddEvent(new CEvPlayerStatus(buffer, length, next, next));
			break;
		case EH_OBJ_BATCH:
			AddEvent(new CEvProjectileBatch(buffer, length, next, next));
			break;
		default:
			return;
		}
	}
}

CProtocol::~CProtocol() {
	for (int i = 0; i < m_EventCount; i++) {
		delete m_apEventList[i];
		m_apEventList[i] = NULL;
	}
	delete []m_apEventList;
}

void CProtocol::AddEvent(CProtocolEvent* ev)
{
	m_apEventList[m_EventCount] = ev;
	m_EventCount++;
}

void CProtocol::ClearProtocol()
{
	for (int i = 0; i < m_EventCount; i++) {
		delete m_apEventList[i];
		m_apEventList[i] = NULL;
	}
	m_EventCount = 0;
}

int CProtocol::ToByteBuffer(char* buffer, int length) {
	if (length <= 0)
		return -1;

	buffer[0] = BUFFERTYPE_DATA;
	int next = value_to_buffer(buffer, length, 1, m_EventCount);
	for (int i = 0; i < m_EventCount; ++i) {
		next = m_apEventList[i]->CopyToBuffer(buffer, length, next);
		if (next == -1) {
			return -1;
		}
	}
	return next;
}

CProtocolEvent * CProtocol::GetEvent(int index)
{
	if (index < 0 || index >= m_EventCount)
		return NULL;

	return m_apEventList[index];
}

int CProtocol::GetEventCount()
{
	return m_EventCount;
}
