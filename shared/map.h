/*
 * map.h
 *
 *  Created on: Mar 9, 2017
 *      Author: tsfreddie
 */
#ifndef SHARED_MAP_H_
#define SHARED_MAP_H_

class CMap {
private:
	int m_Size;
	int** m_Tiles;
public:
	CMap(int size);
	CMap(char* bytes);
    int** GetTiles(){return m_Tiles;}
	virtual ~CMap();
};

#endif /* SHARED_MAP_H_ */
