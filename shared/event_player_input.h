#ifndef SHARED_EVENT_PLAYER_INPUT_H_
#define SHARED_EVENT_PLAYER_INPUT_H_

#include "event.h"

class CEvPlayerInput : public CProtocolEvent {
private:
	Vector m_Position;
	float m_PointingAngle;
	bool m_Fired;

public:
	CEvPlayerInput(Vector position, float Pangle, bool fire);
	CEvPlayerInput(const char* buffer, int length, int start, int & next);
	CEvPlayerInput();
	~CEvPlayerInput();
	int CopyToBuffer(char* buffer, int length, int start);
	Vector GetVector() { return m_Position; }
	float GetPA() { return m_PointingAngle; }
	bool GetFire() { return m_Fired; }

};

#endif
