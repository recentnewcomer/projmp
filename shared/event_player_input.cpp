
#include "event_player_input.h"

CEvPlayerInput::CEvPlayerInput(Vector position, float Pangle, bool fire) : CProtocolEvent(EH_PL_INPUT) {
	m_Position = position;
	m_PointingAngle = Pangle;
	m_Fired = fire;
}

CEvPlayerInput::CEvPlayerInput() : CEvPlayerInput(Vector(0,0),0,0) {}

CEvPlayerInput::CEvPlayerInput(const char* buffer, int length, int start, int& next) : CEvPlayerInput() {
	if (buffer[start] != EH_PL_INPUT) {
		return;
	}
	next = start + 1;
	next = buffer_to_value(buffer, length, next, &m_Position);
	next = buffer_to_value(buffer, length, next, &m_PointingAngle);
	next = buffer_to_value(buffer, length, next, &m_Fired);
}

CEvPlayerInput::~CEvPlayerInput() {
}

int CEvPlayerInput::CopyToBuffer(char* buffer, int length, int start) {

	int next = CProtocolEvent::CopyToBuffer(buffer, length, start);
	if (next < 0) {
		return -1;
	}

	next = value_to_buffer(buffer, length, next, m_Position);
	next = value_to_buffer(buffer, length, next, m_PointingAngle);
	next = value_to_buffer(buffer, length, next, m_Fired);
	return next;
}
 /* event_player_input.cpp
 *
 *  Created on: Feb 25, 2017
 *      Author: fangzhengwei
*/



