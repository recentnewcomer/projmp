/*
 * event_projectile_batch.cpp
 *
 *  Created on: Mar 15, 2017
 *      Author: tsfreddie
 */

#include <event_projectile_batch.h>
#include <cstring>

CEvProjectileBatch::CEvProjectileBatch(CProjectile* objects, char type, int object_count) : CProtocolEvent(EH_OBJ_BATCH) {
	m_BatchingObjects = objects;
	m_ObjectCount = object_count;
	m_DeserializedObjects = NULL;
}

CEvProjectileBatch::CEvProjectileBatch() : CEvProjectileBatch(NULL, 0, 0) {}

CEvProjectileBatch::~CEvProjectileBatch() {
	if (m_DeserializedObjects != NULL) {
		delete[] m_DeserializedObjects;
	}
}

CEvProjectileBatch::CEvProjectileBatch(const char* buffer, int length, int start, int & next)  : CEvProjectileBatch() {
	if (buffer[start] != EH_OBJ_BATCH) {
		return;
	}

	next = start + 1;

	next = buffer_to_value(buffer, length, next, &m_ObjectCount);

	m_DeserializedObjects = new CProjectile[m_ObjectCount];

	for (int i = 0; i < m_ObjectCount; i++) {
		if (next < 0)
			break;

		int id;
		Vector pos;

		next = buffer_to_value(buffer, length, next, &id);
		next = buffer_to_value(buffer, length, next, &pos);
		m_DeserializedObjects[i].Activate(pos, id, -1);
	}


}

int CEvProjectileBatch::CopyToBuffer(char* buffer, int length, int start) {
	int next = CProtocolEvent::CopyToBuffer(buffer, length, start);
	if (next < 0) {
		return -1;
	}

	int num_active = 0;
	int active_in_buffer_position = next;
	next += sizeof(int);

	for (int i = 0; i < m_ObjectCount; i++) {
		if (next < 0)
			break;
		if (!m_BatchingObjects[i].IsActive())
			continue;
		next = value_to_buffer(buffer, length, next, m_BatchingObjects[i].GetID());
		next = value_to_buffer(buffer, length, next, m_BatchingObjects[i].GetPosition());
		num_active++;
	}
	value_to_buffer(buffer, length, active_in_buffer_position, num_active);

	return next;
}
