/*
 * vector.h
 *
 *  Created on: Feb 20, 2017
 *      Author: fangzhengwei
 */
#ifndef SHARED_VECTOR_H_
#define SHARED_VECTOR_H_

struct Vector {
	float x;
	float y;
	Vector();
	Vector(float x, float y);
	~Vector();
	Vector operator+(const Vector & other) const;
	Vector operator-(const Vector & other) const;
	Vector operator*(float other) const;
	Vector operator/(float other) const;

	// TODO: Overloading += -= *= /= == != operators

	float getLength();
	Vector normalized();
	void normalize();
	int IntX(){ return (int)x; };
	int IntY(){ return (int)y; };
};

inline Vector operator*(float left, const Vector & right) { return right * left; }
inline Vector operator/(float left, const Vector & right) { return right / left; }

#endif /* SHARED_VECTOR_H_ */
