#ifndef CLIENT_SERVER_GAME_H_
#define CLIENT_SERVER_GAME_H_

#include "network_server.h"
#include "tick_manager.h"
#include "player.h"
#include "projectile.h"

class CServerGame {
private:
	int m_TickPerSec;
	int m_LastTick;
	CSocketServer* m_pNetwork;
	CPlayer* m_aPlayers;
	CTickManager m_Ticker;

	CProjectile m_aProjectiles[100];

	bool* m_aPlayerFireState;

	// TODO: Dynamic buffer
	char m_aBuf[SHARED_BUFFER_SIZE];
	void PollData();
	void GameUpdate();
	Vector GetRandomLocation();
public:
	CServerGame(CSocketServer* network, int tps);
	void Start();
	void Update();
	int GetInactiveProjectile();
	void DismissAll();
	virtual ~CServerGame();

};

#endif
