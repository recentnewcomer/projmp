#ifndef NETWORK_SERVER_H_
#define NETWORK_SERVER_H_

#include "network.h"

typedef enum {
	LISTENING,
	SERVER_ERROR,
} ServerState;

struct CNetClient {
	int client_id;
	bool active;
	struct sockaddr_in remote_addr;
	socklen_t addr_len;
};

class CSocketServer : public CSocket {
private:
	struct sockaddr_in m_ServerAddr;
	socklen_t m_AddrSize;
	int m_MaxClients;
	ServerState m_State;
	CNetClient* m_aRemotes;
	int FindClientByAddr(const struct sockaddr_in* addr);
	int GetInactiveClient();
public:
	/**
	Create a server socket. The client management process is simplified.
	Please use "CSocket" if you don't want automated client management.
	@param port The port the socket will be listening to.
	@param max_clients The max number of clients that can connect to this server.
	*/
	CSocketServer(int port, int max_clients);
	~CSocketServer();

	/**
	Simple warpper for recvfrom. Automated to communicate with client using client_id.
	@param buffer The buffer that data goes into.
	@param length The max buffer size.
	@param timeout Blocking time before return a timeout flag. -1 for blocking mode.
	@param client_id The reference of a integer value that will be assigned to the index of the sender client.
	@return ReceiveFlag, see "network.h"
	*/
	ReceiveFlag Receive(char* buffer, int length, int timeout, int& client_id);

	/**
	Simple warpper for sendto. Automated to communicate with client using client_id.
	@param buffer The buffer that data comes from.
	@param length The max buffer size.
	@param client_id The index of the target client.
	@return The original error code of "sendto()"
	*/
	int Send(char* buffer, int length, int client_id);

	/**
	Dismiss the client by sending a farewell beat and set it as inactive.
	[The client data will still be received, but will not be processed without another handshaking. ]
	@param client_id The index of the target client.
	@return Undecided error code
	*/
	int Dismiss(int client_id);

	/**
	@return The number of max client allowed
	*/
	int GetMaxClients() { return m_MaxClients; }
};


#endif /* NETWORK_SERVER_H_ */
