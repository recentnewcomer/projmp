#include "server_game.h"
#include "protocol.h"
#include "events.h"

#include <iostream>
#include <cstring>
#include <random>

CServerGame::CServerGame(CSocketServer* network, int tps) {
	m_pNetwork = network;
	int max_player = m_pNetwork->GetMaxClients();
	m_aPlayers = new CPlayer[max_player];
	m_aPlayerFireState = new bool[max_player];
	m_TickPerSec = tps;
	m_LastTick = 0;
}

CServerGame::~CServerGame() {
	delete[]m_aPlayers;
	delete[]m_aPlayerFireState;
}

void CServerGame::PollData() {
	int client_id;
	std::memset(m_aBuf, 0, SHARED_BUFFER_SIZE);
	ReceiveFlag rc_flag = m_pNetwork->Receive(m_aBuf, SHARED_BUFFER_SIZE, 2, client_id);
	switch (rc_flag) {
	case RF_HANDSHAKE:
		m_aPlayers[client_id].SetPosition(GetRandomLocation());
		m_aPlayers[client_id].Activate(10, client_id);
		std::cout << "Player " << client_id << " joined" << std::endl;
		break;
	case RF_DATA:
	{
		CProtocol prot(m_aBuf, SHARED_BUFFER_SIZE);
		// TODO: correct event handling
		CEvPlayerInput* ev = (CEvPlayerInput*)prot.GetEvent(0);
		if (m_aPlayers[client_id].IsActive() && (!m_aPlayers[client_id].IsDead())) {
			m_aPlayers[client_id].SetPosition(ev->GetVector());
			m_aPlayers[client_id].SetAngle(ev->GetPA());
			bool fired = ev->GetFire();
			if (fired) {
				int pid = GetInactiveProjectile();
				m_aProjectiles[pid].Activate(ev->GetVector(), ev->GetPA(), 25, 1, pid, client_id);
			}
		}
		break;
	}
	case RF_FAREWELL:
		m_aPlayers[client_id].Deactivate();
		std::cout << "Player " << client_id << " left" << std::endl;
		break;
	default:
		break;
	}
}

void CServerGame::GameUpdate() {
	int max_player = m_pNetwork->GetMaxClients();

	// TODO: Add Invisibility frames

	for (int i = 0; i < 100; i++) {
		if (m_aProjectiles[i].IsActive()) {
			m_aProjectiles[i].Update(1.0 / m_TickPerSec);
			// TODO: omg, this is bad.
			for (int j = 0; j < max_player; j++) {
				if ((m_aPlayers[j].IsActive() && (!m_aPlayers[j].IsDead())) && (m_aProjectiles[i].GetOwnerID() != j) && m_aProjectiles[i].Hit(m_aPlayers[j])) {
					std::cout << "Player " << m_aProjectiles[i].GetOwnerID() << " hit Player " << j << std::endl;
					m_aProjectiles[i].Deactivate();
					m_aPlayers[j].TakeDamage();
					if (m_aPlayers[j].IsDead()) {
						std::cout << "Player " <<  j << " died" << std::endl;
						m_aPlayers[j].SetGeneralTimer(2);
					}
					break;
				}
			}
		}
	}


	CProtocol prot(max_player+1);
	for (int i = 0; i < max_player; i++) {
		if (m_aPlayers[i].GetGeneralTimer() > 0) {
			m_aPlayers[i].SetGeneralTimer(m_aPlayers[i].GetGeneralTimer() - (1.0 / m_TickPerSec));
			if (m_aPlayers[i].GetGeneralTimer() <= 0) {
				std::cout << "Player " << i << " respawned" << std::endl;
				m_aPlayers[i].SetPosition(GetRandomLocation());
				m_aPlayers[i].SetStatus(3);
			}
		}

		prot.AddEvent(new CEvPlayerStatus(
			m_aPlayers[i].GetClientID(),
			m_aPlayers[i].GetPosition(),
			m_aPlayers[i].GetStatus(),
			m_aPlayers[i].GetAngle(),
			m_aPlayers[i].IsActive(),
			m_aPlayers[i].GetGeneralTimer() > 0
		));
	}

	prot.AddEvent(new CEvProjectileBatch(m_aProjectiles, 1, 100));

	int length = prot.ToByteBuffer(m_aBuf, SHARED_BUFFER_SIZE);

	if (length < 0)
		return;

	// Boardcasting
	for (int i = 0; i < max_player; i++) {
		if (m_aPlayers[i].IsActive()){
			m_pNetwork->Send(m_aBuf, length, i);
		}
	}
}

Vector CServerGame::GetRandomLocation()
{
	float x = (rand() % 5000 / 100) - 25;
	float y = (rand() % 5000 / 100) - 25;
	return Vector(x,y);
}

void CServerGame::Start() {
	m_Ticker.StartClock();
	std::cout << "Server Started" << std::endl;
}

void CServerGame::Update() {
	int this_tick = m_Ticker.GetTickCount(m_TickPerSec);
	PollData();
	if (this_tick != m_LastTick) {
		GameUpdate();
		m_LastTick = this_tick;
	}
}

int CServerGame::GetInactiveProjectile()
{
	for (int i = 0; i < 100; i++) {
		if (!m_aProjectiles[i].IsActive())
		{
			return i;
		}
			
	}
	return -1;
}

void CServerGame::DismissAll()
{
	int max_player = m_pNetwork->GetMaxClients();
	for (int i = 0; i < max_player; i++) {
		if (m_aPlayers[i].IsActive()) {
			m_pNetwork->Dismiss(i);
		}
	}
}

