#include "network_server.h"
#include "server_game.h"
#include <iostream>

#ifndef Windows
/**
Linux (POSIX) implementation of _kbhit().
Morgan McGuire, morgan@cs.brown.edu
*/
#include <stdio.h>
#include <sys/ioctl.h>
#include <termios.h>

int _kbhit() {
	static const int STDIN = 0;
	static bool initialized = false;

	if (!initialized) {
		// Use termios to turn off line buffering
		termios term;
		tcgetattr(STDIN, &term);
		term.c_lflag &= ~ICANON;
		tcsetattr(STDIN, TCSANOW, &term);
		setbuf(stdin, NULL);
		initialized = true;
	}

	int bytesWaiting;
	ioctl(STDIN, FIONREAD, &bytesWaiting);
	return bytesWaiting;
}

char _getch() {
	return getchar();
}

#else
#include <conio.h>
#include <Windows.h>
#endif

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		std::cout << "Usage: server [port]" << std::endl;
		std::cout << "Example: " << argv[0] << " 9365" << std::endl;
		return 1;
	}


	int port = atoi(argv[1]);

	net_init();

	CSocketServer ss(port, 16);
	CServerGame sg(&ss, 60);

	sg.Start();

	bool quit = false;

	while (!quit) {
		sg.Update();
		if (_kbhit() && _getch() == 'q') {
			
			quit = true;
		}
	}


	return 0;
}
