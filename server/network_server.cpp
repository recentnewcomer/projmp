#include "network_server.h"
#include <cstring>

CSocketServer::CSocketServer(const int port, const int max_clients) : CSocket() {
	m_ServerAddr.sin_family = AF_INET;
	m_ServerAddr.sin_port = htons(port);
    m_ServerAddr.sin_addr.s_addr = INADDR_ANY;
	std::memset(m_ServerAddr.sin_zero, '\0', sizeof m_ServerAddr.sin_zero);
	m_AddrSize = sizeof(m_ServerAddr);
	if (bind(GetSocket(), (struct sockaddr*) &m_ServerAddr, m_AddrSize) < 0) {
		m_State = ServerState::SERVER_ERROR;
		return;
	}

	m_aRemotes = new CNetClient[max_clients];
	for (int i = 0; i < max_clients; i++) {
		m_aRemotes[i].client_id = i;
		m_aRemotes[i].active = false;
	}
	m_MaxClients = max_clients;
}

CSocketServer::~CSocketServer() {
	delete m_aRemotes;
}

ReceiveFlag CSocketServer::Receive(char* buffer, int length, int timeout, int& client_id) {
	ReceiveFlag flag = ReceiveFrom(buffer, length, timeout);

	if (flag & RF_SIMPLE) {
		struct sockaddr_in* addr = GetFromAddr();
		socklen_t addr_len = GetFromAddrSize();
		int cid = FindClientByAddr(addr);
		if (cid > -1) {
			if (m_aRemotes[cid].active == false) {
				// The client is already been dropped.
				client_id = -1;
				return RF_DROPED | RF_ERROR;
			}

			client_id = cid;

			// TODO: Check buffer length

			if (buffer[0] == BUFFERTYPE_BEAT && buffer[1] == BT_FAREWELL) {
				m_aRemotes[cid].active = false;
				client_id = cid;
				return RF_FAREWELL;
			}

			if (buffer[0] == BUFFERTYPE_DATA) {
				return RF_DATA;
			}

			return RF_DROPED | RF_ERROR;
		}
		// Full
		if (cid == -2) {
			return RF_DROPED | RF_FULL;
		}

		// New client cid == -1
		cid = GetInactiveClient();
		if (cid < 0) {
			SendBeatBuffer(BT_REJECTED, (struct sockaddr*)addr, addr_len);
			return RF_DROPED | RF_ERROR;
		}

		if (length <= 2)
			return RF_DROPED | RF_ERROR;


		if (buffer[0] == BUFFERTYPE_BEAT && buffer[1] == BT_HANDSHAKE) {
			copy_addr(addr, addr_len, &m_aRemotes[cid].remote_addr, m_aRemotes[cid].addr_len);
			m_aRemotes[cid].active = true;
			SendBeatBuffer(BT_ACCEPTED, cid, (struct sockaddr*)addr, addr_len);
			client_id = cid;
			return RF_HANDSHAKE;
		}
	}

	return flag;
}

int CSocketServer::Send(char * buffer, int length, int client_id)
{
	if (m_aRemotes[client_id].active == false) {
		return -1;
	}
	return SendTo(buffer, length, (struct sockaddr*)&m_aRemotes[client_id].remote_addr, m_aRemotes[client_id].addr_len);
}

int CSocketServer::Dismiss(int cid)
{
	if (m_aRemotes[cid].active) {
		m_aRemotes[cid].active = false;
		SendBeatBuffer(BT_FAREWELL, (struct sockaddr*)&m_aRemotes[cid].remote_addr, m_aRemotes[cid].addr_len);
		return 1;
	}
	return 0;
}

int CSocketServer::FindClientByAddr(const sockaddr_in* addr) {
	for (int i = 0; i < m_MaxClients; i++) {
		if (m_aRemotes[i].active && is_same_addr(&m_aRemotes[i].remote_addr, addr))
			return i;
	}
	return -1;
}

int CSocketServer::GetInactiveClient() {
	for (int i = 0; i < m_MaxClients; i++) {
		if (!m_aRemotes[i].active) {
			return i;
		}
	}
	return -1;
}
