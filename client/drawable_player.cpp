#include "drawable_player.h"
#include "sdl_backend.h"
#include <cstring>

CDrawablePlayer::CDrawablePlayer() :
	CPlayer(),
	CDrawable(2, DT_TEXTURE) {
	char draw_arg[8];
	draw_arg[0] = 0;
	draw_arg[1] = 0;
	draw_arg[2] = 0;
	draw_arg[3] = TX_CHAR1;
	float rotation = 30.0;
	std::memcpy(draw_arg+4, &rotation, 4);

	SetDrawArg(draw_arg);
}

CDrawablePlayer::~CDrawablePlayer(){}

Vector CDrawablePlayer::GetVectorArg1(){
	return m_Position - Vector(1.5, 1.5);
}

Vector CDrawablePlayer::GetVectorArg2(){
    return Vector(3,3);
}

char * CDrawablePlayer::GetDrawArg()
{
	char draw_arg[8];
	draw_arg[0] = 255;
	draw_arg[1] = 255;
	draw_arg[2] = 255;
	draw_arg[3] = 3-m_Status;
	float rotation = GetAngle();
	std::memcpy(draw_arg + 4, &rotation, 4);

	SetDrawArg(draw_arg);
	return CDrawable::GetDrawArg();
}

bool CDrawablePlayer::GetNoDrawFlag() {
	return m_IsDead || (!m_IsActive);
}

