#ifndef NETWORK_CLIENT_H_
#define NETWORK_CLIENT_H_

#include "network.h"

typedef enum {
	CS_IDLE,
	CS_HANDSHAKING,
	CS_CONNECTED,
	CS_ERROR,
} ClientState;


class CSocketClient : public CSocket {
private:
	int m_ClientID;
	ClientState m_State;
	struct sockaddr_in m_ServerAddr;
	socklen_t m_AddrSize;
public:
	/**
	Create a client socket. This will create a simple socket without any target info.
	Set the server target using "Connect()" method.
	Please use "CSocket" if you don't want automated server targeting.
	*/
	CSocketClient();
	~CSocketClient();

	/**
	Send a handshake packet to the specific server and remember the host for future communication.
	@param host The sockaddr of the server.
	@param port The target port.
	@return 1 if any error occured.
	*/
	int Connect(const struct sockaddr* host, int port);

	/**
	Simple warpper for recvfrom. Automated to receive data from server only.
	@param buffer The buffer that data goes into.
	@param length The max buffer size.
	@param timeout Blocking time before return a timeout flag. -1 for blocking mode.
	@return ReceiveFlag, see "network.h"
	*/
	ReceiveFlag Receive(char* buffer, int length, int timeout);

	/**
	Simple warpper for sendto. Send the data to server.
	@param buffer The buffer that data comes from.
	@param length The max buffer size.
	@return The original error code of "sendto()"
	*/
	int Send(char* buffer, int length);


	/**
	Send a Farewell beat to let the server know we will be disconnected.
	@return 1 if any error occured.
	*/
	int Disconnect();

	int GetLocalClientID() { return m_ClientID; }
};

#endif /* NETWORK_CLIENT_H_ */
