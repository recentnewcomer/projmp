/*
 * game.cpp
 *
 *  Created on: Mar 11, 2017
 *      Author: tsfreddie
 */

#include "client_game.h"
#include "protocol.h"
#include "events.h"

#include <math.h>
#include <cstring>
#include <iostream>

CClientGame::CClientGame(CSocketClient * network, CCamera* camera) {
	m_MaxPlayer = 0;
	m_aPlayers = NULL;
	m_pInput = NULL;
	m_pNetwork = network;
	m_pCamera = camera;
	m_LocalFireState = false;
	m_FireInterval = FIRE_INTERVAL;
}

bool CClientGame::Start(int max_player) {
	m_pInput = new CInputManager();

	m_MaxPlayer = max_player;
	m_aPlayers = new CDrawablePlayer[max_player];

	m_Ticker.StartClock();
	return true;
}

bool CClientGame::Update() {
	float deltaTime = m_Ticker.GetDeltaTime() / 1000;
	m_pInput->Update();
	if (m_pInput->IsExited())
		return true;

	bool exit = PollingUpdate(deltaTime);
	LocalUpdate(deltaTime);

	if (m_pInput->IsKey(AK_ESCAPE)) {
		exit = true;
	}
	return exit;
}

bool CClientGame::PollingUpdate(float deltaTime) {
	std::memset(m_aBuf, 0, SHARED_BUFFER_SIZE);
	int localCID = m_pNetwork->GetLocalClientID();
	ReceiveFlag rc_flag = m_pNetwork->Receive(m_aBuf, SHARED_BUFFER_SIZE, 0);
	if (rc_flag & RF_DATA) {
		CProtocol prot(m_aBuf, SHARED_BUFFER_SIZE);
		// TODO: correct event handling
		for (int i = 0; i < prot.GetEventCount(); i++) {
			CProtocolEvent* e = prot.GetEvent(i);
			switch (e->GetEventHeader()) {
			case EH_PL_STATUS:
			{
				CEvPlayerStatus* ev = (CEvPlayerStatus*)e;
				int cid = ev->GetClientID();
				if (!ev->IsConnected()) {
					if (m_aPlayers[cid].IsActive()) {
						m_aPlayers[cid].Deactivate();
					}
					continue;
				}
				if (cid != localCID || m_aPlayers[cid].IsDead()) {
					m_aPlayers[cid].SetPosition(ev->GetPosition());
					m_aPlayers[cid].SetAngle(ev->GetAngle());
				}
				m_aPlayers[cid].SetStatus(ev->GetHP());
				if (ev->IsConnected() && (!m_aPlayers[cid].IsActive())) {
					m_aPlayers[cid].SetPosition(ev->GetPosition());
					m_aPlayers[cid].Activate(10, cid);
					std::cout << "Playes " << cid << " activated" << std::endl;
				}
				break;
			}
			case EH_OBJ_BATCH:
			{
				for (int i = 0; i < 100; i++) {
					m_aProjectiles[i].Deactivate();
				}
				CEvProjectileBatch* ev = (CEvProjectileBatch*)e;
				int num_objects = ev->GetObjectCount();
				CProjectile* objects = ev->GetObjects();
				for (int i = 0; i < num_objects; i++) {
					int id = objects[i].GetID();
					Vector pos = objects[i].GetPosition();
					m_aProjectiles[id].Activate(pos, i, -1);
				}
				break;
			}
			default:
				break;
			}
		}
	}

	if (rc_flag & RF_FAREWELL) {
		std::cout << "Farewell" << std::endl;
		return true;
	}


	// Respond to server
	CProtocol prot(2);
	prot.AddEvent(new CEvPlayerInput(m_aPlayers[localCID].GetPosition(), m_aPlayers[localCID].GetAngle(), m_LocalFireState));
	int length = prot.ToByteBuffer(m_aBuf, SHARED_BUFFER_SIZE);
	m_pNetwork->Send(m_aBuf, length);

	m_LocalFireState = false;

	return false;
}

void CClientGame::LocalUpdate(float deltaTime) {
	// TOOD: Do game logic

	//------- TIMEING
	m_FireInterval -= deltaTime;
	if (m_FireInterval < 0) {
		m_FireInterval = 0;
	}

	//------- MOVEMENT
    int cid = m_pNetwork->GetLocalClientID();

	// Invalid CID
    if (cid < 0)
    	return;

	// Don't move when dead
	if (m_aPlayers[cid].IsDead())
		return;

    Vector move(0,0);
    if(m_pInput->IsKey(AK_UP)){
        move = move+Vector(0,-1);
    }
    if(m_pInput->IsKey(AK_DOWN)){
        move = move+Vector(0,1);
    }
    if(m_pInput->IsKey(AK_LEFT)){
        move = move+Vector(-1,0);
    }
    if(m_pInput->IsKey(AK_RIGHT)){
        move = move+Vector(1,0);
    }
    m_aPlayers[cid].Move((move)*deltaTime*m_aPlayers[cid].GetSpeed());

	if (m_aPlayers[cid].GetPosition().x > 30) m_aPlayers[cid].SetPosition(Vector(30, m_aPlayers[cid].GetPosition().y));
	if (m_aPlayers[cid].GetPosition().x < -30) m_aPlayers[cid].SetPosition(Vector(-30, m_aPlayers[cid].GetPosition().y));
	if (m_aPlayers[cid].GetPosition().y > 30) m_aPlayers[cid].SetPosition(Vector(m_aPlayers[cid].GetPosition().x, 30));
	if (m_aPlayers[cid].GetPosition().y < -30) m_aPlayers[cid].SetPosition(Vector(m_aPlayers[cid].GetPosition().x, -30));

	Vector player_pos = m_aPlayers[cid].GetPosition();
	Vector mouse_pos = m_pCamera->ScreenToWorld(Vector(m_pInput->GetMouseX(), m_pInput->GetMouseY()));
	Vector diff = mouse_pos - player_pos;
	float angle = atan2f(diff.x, diff.y) * 180.0 / M_PI;
	
	m_aPlayers[cid].SetAngle(-angle);

	m_pCamera->SetCenter(m_aPlayers[cid].GetPosition());

	//------- FIRE
	if (m_FireInterval <= 0 && m_pInput->IsKey(AK_FIRE)) {
		m_FireInterval = FIRE_INTERVAL;
		m_LocalFireState = true;
	}

}

void CClientGame::AddDrawables(CDrawingManager * drawer) {
	drawer->Clear(0);
	for (int i = 0; i < m_MaxPlayer; i++) {
		drawer->AddDrawable(&m_aPlayers[i]);
	}
	for (int i = 0; i < 100; i++) {
		drawer->AddDrawable(&m_aProjectiles[i]);
	}
}

CClientGame::~CClientGame() {
	delete m_pInput;

#ifndef Windows
	// TODO: Fix this
	if(m_aPlayers != NULL)
		delete[] m_aPlayers;
#endif
}

