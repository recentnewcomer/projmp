#ifndef CLIENT_DRAWABLE_PLAYER_H_
#define CLIENT_DRAWABLE_PLAYER_H_

#include "vector.h"
#include "drawable.h"
#include "player.h"

class CDrawablePlayer : public CPlayer, public CDrawable {
public:
	CDrawablePlayer();
	~CDrawablePlayer();
    //Drawable
	Vector GetVectorArg1();
	Vector GetVectorArg2();
	char* GetDrawArg();
	bool GetNoDrawFlag();
};

#endif
