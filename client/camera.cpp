
#include "camera.h"

CCamera::CCamera(int screen_width, int screen_height, int ref_width, int ref_height, float unit_on_ref) {
	m_ScreenWidth = screen_width;
	m_ScreenHeight = screen_height;
	m_ReferenceWidth = ref_width;
	m_ReferenceHeight = ref_height;
	m_UnitLength = unit_on_ref;
	m_Scale = screen_height / (ref_height / unit_on_ref);
	m_ScreenCenter = Vector(screen_width / 2.0, screen_height / 2.0);
	m_Center = Vector(0,0);
}

CCamera::~CCamera() {

}

Vector CCamera::ScreenToWorld(Vector vec) {
	Vector fromTopLeft = vec - m_ScreenCenter;
	fromTopLeft = fromTopLeft / m_Scale;
	return fromTopLeft + m_Center;
}

Vector CCamera::WorldToScreen(Vector vec) {
	Vector diff = vec - m_Center;
	diff = diff * m_Scale;
	return diff + m_ScreenCenter;
}

bool CCamera::IsOnScreen(Vector vec)
{
	// TODO: Determine if the vector is on screen
    Vector diff = m_Center - vec;
    if (diff.x > m_ScreenWidth/2 || diff.x < (0 - m_ScreenWidth/2) ){
        return false;
    }
    else if (diff.y > m_ScreenHeight/2 || diff.y < (0 - m_ScreenHeight/2)){
        return false;
    }
    
    
	return true;
}

void CCamera::SetCenter(Vector center){
    m_Center =  center;
}
