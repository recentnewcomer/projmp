/*
 * drawable.h
 *
 *  Created on: Mar 9, 2017
 *      Author: tsfreddie
 */
#ifndef CLIENT_DRAWABLE_H_
#define CLIENT_DRAWABLE_H_

#include "vector.h"

typedef enum {
	DT_LINE,
	DT_RECT,
	DT_TEXTURE,
	DT_UNDEFINED,
} DrawType;

/*
DT_LINE:
	VectorArg1 - Point1
	VectorArg2 - Point2
	DrawArgs -
	          0           1           2           3           4           5           6           7
		[     R     |     G     |     B     |           |           |           |           |           ]
DT_RECT:
	VectorArg1 - Position
	VectorArg2 - Size
	DrawArgs -
	          0           1           2           3           4           5           6           7
		[     R     |     G     |     B     |           |           |           |           |           ]
DT_TEXTURE:
	VectorArg1 - Position
	VectorArg2 - Size
	DrawArgs -
	          0           1           2           3           4           5           6           7
		[     R     |     G     |     B     | TextureID |   float   |   float   |   float   |   float   ]
		 \----------- Blending ------------/             \----------------- Rotation ------------------/
*/

class CDrawable {
private:
	int m_RenderOrder;
	DrawType m_Type;
	char* m_DrawArgs;
public:
	CDrawable();
	CDrawable(int renderOrder, DrawType type);
	virtual ~CDrawable();
	int GetRenderOrder() { return m_RenderOrder; }
	void SetRenderOrder(int renderOrder) { m_RenderOrder = renderOrder; }
	void SetDrawArg(char* drawArgs);
	DrawType GetDrawType() { return m_Type; }
	virtual Vector GetVectorArg1() = 0;
	virtual Vector GetVectorArg2() = 0;
	virtual char* GetDrawArg() { return m_DrawArgs; }
	virtual bool GetNoDrawFlag() { return false; }
};

#endif /* CLIENT_DRAWABLE_H_ */
