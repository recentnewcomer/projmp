#include "network_client.h"
#include "event.h"
#include <cstring>
#include <iostream>

CSocketClient::CSocketClient() : CSocket() {
	m_ClientID = -1;
	m_State = CS_IDLE;
	m_AddrSize = sizeof(m_ServerAddr);
}

CSocketClient::~CSocketClient() {
	m_State = CS_IDLE;
}

int CSocketClient::Connect(const struct sockaddr* host, const int port) {
	if (m_State != CS_IDLE && m_State != CS_ERROR)
		return 1;

	if (host == NULL) {
		m_State = CS_ERROR;
		return 1;
	}

	m_ServerAddr.sin_family = AF_INET;
	m_ServerAddr.sin_port = htons(port);
	std::memcpy(&m_ServerAddr.sin_addr.s_addr, &((struct sockaddr_in*)host)->sin_addr.s_addr, 4);
	std::memset(m_ServerAddr.sin_zero, '\0', sizeof m_ServerAddr.sin_zero);

	m_AddrSize = sizeof(m_ServerAddr);

	m_State = CS_HANDSHAKING;
	// Send Handshaking message
	int error = SendBeatBuffer(BT_HANDSHAKE, (struct sockaddr*)&m_ServerAddr, m_AddrSize);
	if (error == -1) {
		m_State = CS_ERROR;
		return 1;
	}

	return 0;
}

ReceiveFlag CSocketClient::Receive(char * buffer, int length, int timeout)
{
	ReceiveFlag flag = ReceiveFrom(buffer, length, timeout);

	if (flag & RF_SIMPLE) {
		// Shouldn't receive any packet with error state.
		if (m_State == CS_ERROR)
			return RF_DROPED | RF_ERROR;

		struct sockaddr_in* addr = GetFromAddr();

		// Shouldn't receive any packet from anyone but the server.
		if (!is_same_addr(addr, &m_ServerAddr))
			return RF_DROPED | RF_ERROR;

		// TODO: Check buffer length

		if (buffer[0] == BUFFERTYPE_BEAT && buffer[1] == BT_ACCEPTED) {
			m_State = CS_CONNECTED;
			buffer_to_value(buffer, BEATBUFFER_SIZE, 2, &m_ClientID);
			return RF_HANDSHAKE;
		}

		if (buffer[0] == BUFFERTYPE_BEAT && buffer[1] == BT_FAREWELL) {
			m_State = CS_IDLE;
			return RF_FAREWELL;
		}

		if (buffer[0] == BUFFERTYPE_BEAT && buffer[1] == BT_REJECTED) {
			m_State = CS_IDLE;
			return RF_DROPED | RF_DENIED;
		}

		if (buffer[0] == BUFFERTYPE_DATA) {
			if (m_State != CS_CONNECTED)
				return RF_DROPED | RF_ERROR;
			return RF_DATA;
		}
	}

	return flag;
}

int CSocketClient::Send(char * buffer, int length)
{
	return SendTo(buffer, length, (struct sockaddr*) &m_ServerAddr, m_AddrSize);
}

int CSocketClient::Disconnect() {
	int error = 0;
	if (SendBeatBuffer(BT_FAREWELL, (struct sockaddr*)&m_ServerAddr, m_AddrSize))
		error = 1;
	return error;
}
