#ifndef CLIENT_DRAWABLE_PROJECTILE_H_
#define CLIENT_DRAWABLE_PROJECTILE_H_

#include "vector.h"
#include "drawable.h"
#include "projectile.h"

class CDrawableProjectile : public CProjectile, public CDrawable {
public:
	CDrawableProjectile();
	~CDrawableProjectile();
	//Drawable
	Vector GetVectorArg1();
	Vector GetVectorArg2();
	bool GetNoDrawFlag();
};

#endif
