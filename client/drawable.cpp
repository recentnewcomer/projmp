/*
 * drawable.cpp
 *
 *  Created on: Mar 9, 2017
 *      Author: tsfreddie
 */

#include "drawable.h"
#include <cstring>

CDrawable::CDrawable() {
	m_RenderOrder = 0;
	m_Type = DT_UNDEFINED;
	m_DrawArgs = new char[8];
	std::memset(m_DrawArgs, 0, 8);
}

CDrawable::CDrawable(int renderOrder, DrawType type) {
	m_RenderOrder = renderOrder;
	m_Type = type;
	m_DrawArgs = new char[8];
	std::memset(m_DrawArgs, 0, 8);
}

void CDrawable::SetDrawArg(char* drawArgs) {
	std::memcpy(m_DrawArgs, drawArgs, 8);
}

CDrawable::~CDrawable() {
	delete[] m_DrawArgs;
}





