#ifndef CAMERA_H_
#define CAMERA_H_

#include "vector.h"

class CCamera {
private:
	int m_ScreenWidth;
	int m_ScreenHeight;
	int m_ReferenceWidth;
	int m_ReferenceHeight;
	Vector m_ScreenCenter;
	float m_Scale;
	float m_UnitLength;
    Vector m_Center;
    
public:
    CCamera(int screen_width, int screen_height, int ref_width, int ref_height, float unit_on_ref);
    virtual ~CCamera();
    void SetCenter(Vector center);
    Vector GetCenter(){return m_Center; };
    Vector ScreenToWorld(Vector vec);
    Vector WorldToScreen(Vector vec);
	bool IsOnScreen(Vector vec);
   
};

#endif
