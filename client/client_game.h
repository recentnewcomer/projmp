/*
 * client_game.h
 *
 *  Created on: Mar 11, 2017
 *      Author: tsfreddie
 */
#ifndef CLIENT_CLIENT_GAME_H_
#define CLIENT_CLIENT_GAME_H_

#include "input_manager.h"
#include "drawing_manager.h"
#include "network_client.h"
#include "tick_manager.h"

#include "drawable_player.h"
#include "drawable_projectile.h"

#define FIRE_INTERVAL 0.25;

class CClientGame {
private:
	CSocketClient* m_pNetwork;
	CDrawablePlayer* m_aPlayers;
	CInputManager* m_pInput;
	CTickManager m_Ticker;
	CCamera* m_pCamera;

	char m_aBuf[SHARED_BUFFER_SIZE];

	CDrawableProjectile m_aProjectiles[100];
	
	bool m_LocalFireState;
	float m_FireInterval;
	
	int m_MaxPlayer;
	/**
	Update entities/objects if any there is avaliable packet
	*/
	bool PollingUpdate(float deltaTime);
	/**
	Update local objects from user input
	*/
	void LocalUpdate(float deltaTime);
public:
	CClientGame(CSocketClient * network, CCamera* camera);
	/**
	Start ticking and prepare game entities.
	@param max_player Max number of player allowed
	@return true if the game started successfully
	*/
	bool Start(int max_player);
	/**
	Game update
	@return true if the game need to be quit
	*/
	bool Update();
	/**
	Add all drawble objects into DrawingManager
	@param drawer The DrawingManager that need to be filled
	*/
	void AddDrawables(CDrawingManager* drawer);
	
	virtual ~CClientGame();
};

#endif /* CLIENT_GAME_H_ */
