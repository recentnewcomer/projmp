#include <iostream>
#include <SDL2/SDL.h>

#include "client_game.h"
#include "sdl_backend.h"
#include "network_client.h"
#include "drawing_manager.h"

#include <cstring>


#ifdef Windows
#include <Windows.h>
#endif

int main(int argc, char* argv[]) {
	if (argc < 3)
	{
		std::cout << "Usage: client [host] [port] ([screen_width] [screen_height])" << std::endl;
		std::cout << "Example: " << argv[0] << " localhost 9365" << std::endl;
		return 1;
	}

	// TODO: Factor out connecting

	char* host = argv[1];
	int port = atoi(argv[2]);

	net_init();

	struct addrinfo hints;
	struct addrinfo* result;

	std::memset(&hints, '\0' ,sizeof(hints));

	hints.ai_family = AF_INET;

	int e = getaddrinfo(host, NULL, &hints, &result);
	if (e != 0 || !result) {
		std::cout << "No such host" << std::endl;
		return 1;
	}

	CSocketClient socket;

	socket.Connect(result->ai_addr, port);

	freeaddrinfo(result);

	std::cout << "Connecting" << std::endl;
	char buffer[BEATBUFFER_SIZE];
	
	ReceiveFlag rf = socket.Receive(buffer, 255, -1);
	
	if (rf & RF_DENIED) {
		std::cout << "Connection failed: Server is full" << std::endl;
		return 1;
	}

	if (!(rf & RF_HANDSHAKE)) {
		std::cout << "Connection failed: Unknown error" << std::endl;
		return 1;
	}

	std::cout << "Connected" << std::endl;

	int screen_width = 800;
	int screen_height = 600;

	if (argc >= 5) {
		screen_width = atoi(argv[3]);
		screen_height = atoi(argv[4]);
	}

	CSDLBackend sdl("Test", screen_width, screen_height);

	CClientGame game(&socket, sdl.GetMainCamera());
	CDrawingManager drawer(16 + 200);
	// TODO: do network stuff
	game.Start(16);

	bool quit = false;

	while (!quit) {
		sdl.Clear();
		quit = game.Update();
		game.AddDrawables(&drawer);
		drawer.Draw(&sdl);
		sdl.Present();
	}

	socket.Disconnect();

	return 0;
}
