/*
 * drawing_manager.h
 *
 *  Created on: Mar 9, 2017
 *      Author: tsfreddie
 */
#ifndef CLIENT_DRAWING_MANAGER_H_
#define CLIENT_DRAWING_MANAGER_H_

#include "drawable.h"
#include "sdl_backend.h"

class CDrawingManager {
private:
	CDrawable** m_apDrawables;
	int m_MaxObjects;
	int m_ObjectCount;
public:
	CDrawingManager(int max_objects);
	virtual ~CDrawingManager();
	void AddDrawable(const CDrawable* pDrawable);
	void Draw(CSDLBackend* pSDL);
	void Clear(int fixed_objects);
};

#endif /* CLIENT_DRAWING_MANAGER_H_ */
