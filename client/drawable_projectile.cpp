#include "drawable_projectile.h"
#include "sdl_backend.h"
#include <cstring>

CDrawableProjectile::CDrawableProjectile() :
	CProjectile(),
	CDrawable(2, DT_TEXTURE) {
	char draw_arg[8];
	draw_arg[0] = 255;
	draw_arg[1] = 255;
	draw_arg[2] = 255;
	draw_arg[3] = TX_POINT;
	std::memset(draw_arg+4, 0, 4);

	SetDrawArg(draw_arg);
}

CDrawableProjectile::~CDrawableProjectile(){}

Vector CDrawableProjectile::GetVectorArg1(){
    return m_Position - Vector(0.2, 0.2);
}

Vector CDrawableProjectile::GetVectorArg2(){
    return Vector(0.4, 0.4);
}

bool CDrawableProjectile::GetNoDrawFlag() {
	return !IsActive();
}
