/*
 * input_manager.h
 *
 *  Created on: Mar 10, 2017
 *      Author: tsfreddie
 */
#ifndef CLIENT_INPUT_MANAGER_H_
#define CLIENT_INPUT_MANAGER_H_

typedef enum {
	AK_UP,
	AK_DOWN,
	AK_LEFT,
	AK_RIGHT,
	AK_FIRE,
	AK_ESCAPE,
	AK_UNDEFINED,
} AvaliableKeys;

class CInputManager {
private:
	int m_KeyState[AK_UNDEFINED];
	int m_MouseX;
	int m_MouseY;
	bool m_Exited;
public:
	CInputManager();

	void Update();
	bool IsKeyDown(AvaliableKeys key);
	bool IsKeyUp(AvaliableKeys key);
	bool IsKey(AvaliableKeys key);
	int GetKeyState(AvaliableKeys key);

	int GetMouseX() { return m_MouseX; }
	int GetMouseY() { return m_MouseY; }

	bool IsExited() { return m_Exited; };

	virtual ~CInputManager();
};

#endif /* CLIENT_INPUT_MANAGER_H_ */
