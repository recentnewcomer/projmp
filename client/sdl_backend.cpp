/*
 * sdl_backend.cpp
 *
 *  Created on: Mar 7, 2017
 *      Author: tsfreddie
 */

#include "sdl_backend.h"
#include <SDL2/SDL_image.h>

#include <iostream>

#define MAP_SIZE 30

CSDLBackend::CSDLBackend(const char* title, int width, int height) {

    //Initialize SDL
    if(SDL_Init(SDL_INIT_VIDEO)<0)
    {
        std::cout<< "SDL could not initialize! SDL Error: " << SDL_GetError() << std::endl;
        return;
    }
	//Create window
	m_pWindow = SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN );
	if(m_pWindow == NULL)
	{
        std::cout<< "Window could not be created! SDL Error: " << SDL_GetError() << std::endl;
		return;
	}

	m_pRenderer = SDL_CreateRenderer(m_pWindow, -1, SDL_RENDERER_SOFTWARE);
	if(m_pRenderer == NULL) {
        std::cout<< "Renderer could not be created! SDL Error: " << SDL_GetError() << std::endl;
		return;
	}

	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "2");

	 //Initialize PNG loading
	int imgFlags = IMG_INIT_PNG;
	if(!(IMG_Init(imgFlags)&imgFlags)) {
		printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
		return;
	}

	// TODO: dynamic texture loading from table [currently hardcoded]
	m_apTextures[TX_CHAR1] = LoadTexture((char*)"data/images/char1.png");
	m_apTextures[TX_CHAR2] = LoadTexture((char*)"data/images/char2.png");
	m_apTextures[TX_CHAR3] = LoadTexture((char*)"data/images/char3.png");
	m_apTextures[TX_BULLET] = LoadTexture((char*)"data/images/bullet.png");
	m_apTextures[TX_POINT] = LoadTexture((char*)"data/images/point.png");


	m_pCamera = new CCamera(width, height, REFERENCE_WIDTH, REFERENCE_HEIGHT, REFERENCE_HEIGHT/20.0);

}


void CSDLBackend::DrawGrid(){
    SetColor(80, 80, 80);
    Vector center = GetMainCamera()->GetCenter();
    
    int farRight = (int)center.x + 30;
    int farBottom = (int)center.y + 15;

    for (int x = (int)center.x - 30; x <= farRight; ++x) {
    	DrawLine(x, center.y - 15, x, center.y + 15);
    }

    for (int y = (int)center.y - 15; y <= farBottom; ++y){
        DrawLine(center.x - 30, y, center.x + 30, y);
    }

	SetColor(255, 0, 0);
	DrawLine(-MAP_SIZE, -MAP_SIZE, -MAP_SIZE, MAP_SIZE);
	DrawLine(-MAP_SIZE, -MAP_SIZE, MAP_SIZE, -MAP_SIZE);
	DrawLine(-MAP_SIZE, MAP_SIZE, MAP_SIZE, MAP_SIZE);
	DrawLine(MAP_SIZE, -MAP_SIZE, MAP_SIZE, MAP_SIZE);
    
}

void CSDLBackend::SetColor(int r, int g, int b) {
	SDL_SetRenderDrawColor(m_pRenderer, r, g, b, SDL_ALPHA_OPAQUE);
}

void CSDLBackend::Clear() {
	SDL_SetRenderDrawColor(m_pRenderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(m_pRenderer);
    DrawGrid();
}

void CSDLBackend::Present() {
	SDL_RenderPresent(m_pRenderer);
}

void CSDLBackend::DrawLine(float x1, float y1, float x2, float y2) {
	Vector lt = m_pCamera->WorldToScreen(Vector(x1, y1));
	Vector rb = m_pCamera->WorldToScreen(Vector(x2, y2));
	SDL_RenderDrawLine(m_pRenderer, lt.IntX(), lt.IntY(), rb.IntX(), rb.IntY());
}

void CSDLBackend::DrawRect(float x, float y, float w, float h) {
	Vector lt = m_pCamera->WorldToScreen(Vector(x, y));
	Vector rb = m_pCamera->WorldToScreen(Vector(x, y)+Vector(w,h));
	Vector scale = rb - lt;
	SDL_Rect rect;
	rect.x = lt.IntX();
	rect.y = lt.IntY();
	rect.w = scale.IntX();
	rect.h = scale.IntY();

	SDL_RenderDrawRect(m_pRenderer, &rect);
}

void CSDLBackend::RenderTexture(char texture_id, float x, float y, float w, float h, float angle, int r, int g, int b) {
	Vector lt = m_pCamera->WorldToScreen(Vector(x, y));
	Vector rb = m_pCamera->WorldToScreen(Vector(x, y) + Vector(w, h));
	Vector scale = rb - lt;
	SDL_Rect rect;
	rect.x = lt.IntX();
	rect.y = lt.IntY();
	rect.w = scale.IntX();
	rect.h = scale.IntY();
	SDL_SetTextureColorMod(m_apTextures[(int)texture_id], r, g, b);
	SDL_RenderCopyEx(m_pRenderer, m_apTextures[(int)texture_id], NULL, &rect, angle, NULL, SDL_FLIP_NONE);
}

void CSDLBackend::FillRect(float x, float y, float w, float h) {
	Vector lt = m_pCamera->WorldToScreen(Vector(x, y));
	Vector rb = m_pCamera->WorldToScreen(Vector(x, y)+Vector(w,h));
	Vector scale = rb - lt;
	SDL_Rect rect;
	rect.x = lt.IntX();
	rect.y = lt.IntY();
	rect.w = scale.IntX();
	rect.h = scale.IntY();

	SDL_RenderFillRect(m_pRenderer, &rect);
}

CSDLBackend::~CSDLBackend() {

	SDL_DestroyRenderer(m_pRenderer);
	m_pRenderer = NULL;
    SDL_DestroyWindow(m_pWindow);
    m_pWindow = NULL;

    delete m_pCamera;
    m_pCamera = NULL;

    //Quit SDL subsystems
    SDL_Quit();
}

SDL_Texture* CSDLBackend::LoadTexture(char* filepath) {
	SDL_Texture* new_texture = NULL;
	SDL_Surface* surface = IMG_Load(filepath);
	if (surface == NULL)
		return NULL;

	new_texture = SDL_CreateTextureFromSurface(m_pRenderer, surface);
	SDL_FreeSurface(surface);
	return new_texture;
}
