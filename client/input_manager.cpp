/*
 * input_manager.cpp
 *
 *  Created on: Mar 10, 2017
 *      Author: tsfreddie
 */

#include "input_manager.h"
#include <SDL2/SDL.h>

CInputManager::CInputManager() {
	m_Exited = false;
	for (int i = 0; i < AK_UNDEFINED; i++) {
		m_KeyState[i] = 0;
	}
}

void CInputManager::Update() {
	SDL_Event e;
	while (SDL_PollEvent(&e) != 0) {
		if (e.type == SDL_QUIT) {
			m_Exited = true;
		}
		if (e.type == SDL_MOUSEMOTION || e.type == SDL_MOUSEBUTTONDOWN || e.type == SDL_MOUSEBUTTONUP) {
			SDL_GetMouseState(&m_MouseX, &m_MouseY);
		}
	}
    const Uint8* currentKeyStates = SDL_GetKeyboardState( NULL );

	// TODO: Hey! Make a table!
    if( currentKeyStates[ SDL_SCANCODE_W ] )
    {
        if(m_KeyState[AK_UP] == 0){
            m_KeyState[AK_UP] = 1;
        }
        else if(m_KeyState[AK_UP] == 1){
            m_KeyState[AK_UP] = 2;
        }
    }
    else {
    	if(m_KeyState[AK_UP] == 2) {
    		m_KeyState[AK_UP] = 3;
    	}
        if(m_KeyState[AK_UP] == 3){
            m_KeyState[AK_UP] = 0;
        }
    }

    if( currentKeyStates[ SDL_SCANCODE_S ] )
    {
        if(m_KeyState[AK_DOWN] == 0){
            m_KeyState[AK_DOWN] = 1;
        }
        else if(m_KeyState[AK_DOWN] == 1){
            m_KeyState[AK_DOWN] = 2;
        }
    }
    else {
    	if(m_KeyState[AK_DOWN] == 2) {
    		m_KeyState[AK_DOWN] = 3;
    	}
        if(m_KeyState[AK_DOWN] == 3){
            m_KeyState[AK_DOWN] = 0;
        }
    }
    if( currentKeyStates[ SDL_SCANCODE_A ] )
    {
        if(m_KeyState[AK_LEFT] == 0){
            m_KeyState[AK_LEFT] = 1;
        }
        else if(m_KeyState[AK_LEFT] == 1){
            m_KeyState[AK_LEFT] = 2;
        }
    }
    else {
    	if(m_KeyState[AK_LEFT] == 2) {
    		m_KeyState[AK_LEFT] = 3;
    	}
        if(m_KeyState[AK_LEFT] == 3){
            m_KeyState[AK_LEFT] = 0;
        }
    }
    if( currentKeyStates[ SDL_SCANCODE_D ] )
    {
        if(m_KeyState[AK_RIGHT] == 0){
            m_KeyState[AK_RIGHT] = 1;
        }
        else if(m_KeyState[AK_RIGHT] == 1){
            m_KeyState[AK_RIGHT] = 2;
        }
    }
    else {
    	if(m_KeyState[AK_RIGHT] == 2) {
    		m_KeyState[AK_RIGHT] = 3;
    	}
        if(m_KeyState[AK_RIGHT] == 3){
            m_KeyState[AK_RIGHT] = 0;
        }
    }

	if (currentKeyStates[SDL_SCANCODE_SPACE])
	{
		if (m_KeyState[AK_FIRE] == 0) {
			m_KeyState[AK_FIRE] = 1;
		}
		else if (m_KeyState[AK_FIRE] == 1) {
			m_KeyState[AK_FIRE] = 2;
		}
	}
	else {
		if (m_KeyState[AK_FIRE] == 2) {
			m_KeyState[AK_FIRE] = 3;
		}
		if (m_KeyState[AK_FIRE] == 3) {
			m_KeyState[AK_FIRE] = 0;
		}
	}

	if (currentKeyStates[SDL_SCANCODE_ESCAPE])
	{
		if (m_KeyState[AK_ESCAPE] == 0) {
			m_KeyState[AK_ESCAPE] = 1;
		}
		else if (m_KeyState[AK_ESCAPE] == 1) {
			m_KeyState[AK_ESCAPE] = 2;
		}
	}
	else {
		if (m_KeyState[AK_ESCAPE] == 2) {
			m_KeyState[AK_ESCAPE] = 3;
		}
		if (m_KeyState[AK_ESCAPE] == 3) {
			m_KeyState[AK_ESCAPE] = 0;
		}
	}
}

CInputManager::~CInputManager() {
	// TODO Auto-generated destructor stub
}

bool CInputManager::IsKeyDown(AvaliableKeys key){
    if (m_KeyState[key] == 1){
        return 1;
    }
    else{ return 0; }
   }

bool CInputManager::IsKeyUp(AvaliableKeys key){
    if (m_KeyState[key] == 3){
        return 1;
    }
    else{ return 0; }
}



bool CInputManager::IsKey(AvaliableKeys key){
    if (m_KeyState[key] == 1 || m_KeyState[key] == 2){
        return 1;
    }
    else{ return 0; }
    
}
int CInputManager::GetKeyState(AvaliableKeys key){
    return m_KeyState[key];
}

















