/*
 * sdl_backend.h
 *
 *  Created on: Mar 7, 2017
 *      Author: tsfreddie
 */
#ifndef CLIENT_SDL_BACKEND_H_
#define CLIENT_SDL_BACKEND_H_

#include <SDL2/SDL.h>

#include "camera.h"
#include "drawable.h"

/*
A not-so-good way to make scaling easy.
*/
#define REFERENCE_WIDTH 800
#define REFERENCE_HEIGHT 600

#define TX_CHAR1 0
#define TX_CHAR2 1
#define TX_CHAR3 2
#define TX_BULLET 3
#define TX_POINT 4

class CSDLBackend {
private:
	SDL_Window* m_pWindow;
	SDL_Renderer* m_pRenderer;
	CCamera* m_pCamera;

	// TODO: Load textures by data
	SDL_Texture* m_apTextures[5];

	bool m_Initialized;

	void DrawGrid();
	SDL_Texture* LoadTexture(char* filepath);

public:
	CSDLBackend(const char* title, int width, int height);
	void SetColor(int r, int g, int b);
	void DrawLine(float x1, float y1, float x2, float y2);
	void FillRect(float x, float y, float w, float h);
	void DrawRect(float x, float y, float w, float h);
	void RenderTexture(char texture_id, float x, float y, float w, float h, float angle, int r = 255, int g = 255, int b = 255);
	void Clear();
	void Present();
    CCamera* GetMainCamera(){return m_pCamera; }
	virtual ~CSDLBackend();
	bool IsInitialized() { return m_Initialized; }
};

#endif /* CLIENT_SDL_BACKEND_H_ */
