/*
 * drawing_manager.cpp
 *
 *  Created on: Mar 9, 2017
 *      Author: tsfreddie
 */

#include "drawing_manager.h"
#include <cstring>

CDrawingManager::CDrawingManager(int max_objects) {
	m_apDrawables = new CDrawable*[max_objects];
	m_MaxObjects = max_objects;
	m_ObjectCount = 0;
}

CDrawingManager::~CDrawingManager() {
	delete []m_apDrawables;
}


void CDrawingManager::AddDrawable(const CDrawable* pDrawable) {
	if (m_ObjectCount >= m_MaxObjects) {
		return;
	}
	m_apDrawables[m_ObjectCount] = (CDrawable*)pDrawable;
	m_ObjectCount++;
}

void CDrawingManager::Clear(int fixed_objects) {
	m_ObjectCount = fixed_objects;
}


void CDrawingManager::Draw(CSDLBackend* pSDL) {
	for (int i = 0; i < m_ObjectCount; i++) {
		// Cancel rendering for NoDraw flag
		if (m_apDrawables[i]->GetNoDrawFlag())
			continue;

		Vector arg1 = m_apDrawables[i]->GetVectorArg1();
		Vector arg2 = m_apDrawables[i]->GetVectorArg2();
		char* src_arg = m_apDrawables[i]->GetDrawArg();

		char draw_arg[8];
		std::memcpy(draw_arg, src_arg, 8);

		int r = (unsigned char)draw_arg[0];
		int g = (unsigned char)draw_arg[1];
		int b = (unsigned char)draw_arg[2];

		switch (m_apDrawables[i]->GetDrawType()) {
		case DT_LINE:
			pSDL->SetColor(r, g, b);
			pSDL->DrawLine(arg1.x, arg1.y, arg2.x, arg2.y);
			break;
		case DT_RECT:
			pSDL->SetColor(r, g, b);
			pSDL->FillRect(arg1.x, arg1.y, arg2.x, arg2.y);
			break;
		case DT_TEXTURE:
		{
			char texture_id = draw_arg[3];
			float rotation;
			std::memcpy(&rotation, draw_arg + 4, 4);
			pSDL->RenderTexture(texture_id, arg1.x, arg1.y, arg2.x, arg2.y, rotation,r,g,b);
			break;
		}
		default:
			break;
		}
	}
}
