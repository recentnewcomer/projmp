/*
 * test_tick_manager.cpp
 *
 *  Created on: Feb 25, 2017
 *      Author: tsfreddie
 */

#include "tick_manager.h"
#include <iostream>

int main() {
	CTickManager tm;
	tm.StartClock();
	int sum = 0;
	for (int i = 0; i < 2000; i++) {
		//task
		sum += i;
	}

	std::cout << sum << std::endl;
	while (1) {
		std::cout << tm.GetTimeSinceStart() << ":" << tm.GetTickCount(1) << std::endl;
	}

}
