/*
 * test_vector.cpp
 *
 *  Created on: Feb 23, 2017
 *      Author: tsfreddie
 */

#include "vector.h"
#include <iostream>

int main() {
	Vector a(1.3, 2.3);
	Vector b(2.1, 3.1);
	Vector c = a + b;
	Vector d = a * 2;
	Vector e = 2 * b;
	std::cout << c.x << " " << c.y << std::endl;
	std::cout << d.x << " " << d.y << std::endl;
	std::cout << e.x << " " << e.y << std::endl;
	std::cout << a.getLength()<< " "<< std::endl;
	std::cout << a.normalized().x << " " << a.normalized().y<< std::endl;
	std::cout << a.normalized().getLength()<< " "<< std::endl;

	return 0;
}
