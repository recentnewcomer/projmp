#include "network_client.h"
#include "events.h"
#include "protocol.h"
#include <iostream>
#include <string>
#include <cstring>
//#include <SDL2/SDL.h>

#ifdef Windows
#include <windows.h>
#endif

#ifdef Windows
int main(int argc, char* argv[]) {
//int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int) {
#else
int main(int argc, char* argv[]) {
#endif

	if (argc < 3)
	{
		std::cout << "Usage: client [host] [port]" << std::endl;
		std::cout << "Example: " << argv[0] << " localhost 9365" << std::endl;
		return 1;
	}

	char* host = argv[1];
	int port = atoi(argv[2]);

	net_init();

	struct addrinfo hints;
	struct addrinfo* result;

	std::memset(&hints, '\0' ,sizeof(hints));

	hints.ai_family = AF_INET;

	int e = getaddrinfo(host, NULL, &hints, &result);
	if (e != 0 || !result) {
		std::cout << "No such host" << std::endl;
		return 1;
	}

	CSocketClient sc;

	sc.Connect(result->ai_addr, port);

	freeaddrinfo(result);

	char buffer[255];

	while (sc.Receive(buffer, 255, -1) != RF_HANDSHAKE);
	std::cout << "Connected" << std::endl;

	while (1) {
		CProtocol prot(2);
		prot.AddEvent(new CEvPlayerInput(Vector(1,1), 0, false));
		int length = prot.ToByteBuffer(buffer, 255);
		sc.Send(buffer, length);
		ReceiveFlag rc_flag = sc.Receive(buffer, 255, 0);
		if (rc_flag | RF_DATA) {
			std::cout << "Hey!" << std::endl;
		}
	}
	return 0;
}
