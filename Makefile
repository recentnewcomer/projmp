#
# structure
#
CLIENT_LABEL = client
SERVER_LABEL = server
SHARED_LABEL = shared
TEST_DIR = test
DATA_DIR = data

DEBUG_DIR = Debug
RELEASE_DIR = Release

#
# settings
#
CC = g++
CFLAGS = -std=c++11 -Wall
DEBUG_FLAG = -g -pg
HEADER_PATH = -Ishared
CLIENT_ONLY_LIBS = -lSDL2 -lSDL2_image
LIBS = -lm

UNAME_S = $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
	CFLAGS += -D OSX
	CLIENT_ONLY_LIBS = -framework SDL2_image -framework SDL2 -F/Library/Frameworks/
	HEADER_PATH += -Iothers/include/
endif
ifeq ($(UNAME_S),Linux)
	CFLAGS += -D Linux
endif
ifneq ($(filter CYGWIN%,$(UNAME_S)),)
	CFLAGS += -D Cygwin
endif
	

#
# paths
#
SOCL = $(CLIENT_LABEL)
SOSV = $(SERVER_LABEL)
SOSH = $(SHARED_LABEL)
OBCLDE = $(CLIENT_LABEL)/$(DEBUG_DIR)
OBCLRE = $(CLIENT_LABEL)/$(RELEASE_DIR)
OBSVDE = $(SERVER_LABEL)/$(DEBUG_DIR)
OBSVRE = $(SERVER_LABEL)/$(RELEASE_DIR)
OBSHDE = $(SHARED_LABEL)/$(DEBUG_DIR)
OBSHRE = $(SHARED_LABEL)/$(RELEASE_DIR)
BIDE = $(DEBUG_DIR)
BIRE = $(RELEASE_DIR)
SOTECL = $(TEST_DIR)/$(CLIENT_LABEL)
SOTESV = $(TEST_DIR)/$(SERVER_LABEL)
SOTESH = $(TEST_DIR)/$(SHARED_LABEL)
OBTE = $(TEST_DIR)/$(DEBUG_DIR)

#
# sources & objects
#

CLIENT_SOURCES = $(wildcard $(SOCL)/*.cpp)
CLIENT_HEADERS = $(wildcard $(SOCL)/*.h)
CLIENT_OBJECTS = $(patsubst $(SOCL)/%.cpp, $(OBCLDE)/%.o, $(CLIENT_SOURCES))
CLIENT_TARGET = $(BIDE)/$(CLIENT_LABEL)
CLIENT_OBJECTS_RELEASE = $(patsubst $(SOCL)/%.cpp, $(OBCLRE)/%.o, $(CLIENT_SOURCES))
CLIENT_TARGET_RELEASE = $(BIRE)/$(CLIENT_LABEL)

SERVER_SOURCES = $(wildcard $(SOSV)/*.cpp)
SERVER_HEADERS = $(wildcard $(SOSV)/*.h)
SERVER_OBJECTS = $(patsubst $(SOSV)/%.cpp, $(OBSVDE)/%.o, $(SERVER_SOURCES))
SERVER_TARGET = $(BIDE)/$(SERVER_LABEL)
SERVER_OBJECTS_RELEASE = $(patsubst $(SOSV)/%.cpp, $(OBSVRE)/%.o, $(SERVER_SOURCES))
SERVER_TARGET_RELEASE = $(BIRE)/$(SERVER_LABEL)

SHARED_SOURCES = $(wildcard $(SOSH)/*.cpp)
SHARED_HEADERS = $(wildcard $(SOSH)/*.h)
SHARED_OBJECTS = $(patsubst $(SOSH)/%.cpp, $(OBSHDE)/%.o, $(SHARED_SOURCES))
SHARED_OBJECTS_RELEASE = $(patsubst $(SOSH)/%.cpp, $(OBSHRE)/%.o, $(SHARED_SOURCES))

TESTCL_SOURCES = $(wildcard $(SOTECL)/test_*.cpp)
TESTCL_OBJECTS = $(patsubst $(SOTECL)/%.cpp, $(OBTE)/%.o, $(TESTCL_SOURCES))
TESTCL_TARGETS = $(patsubst $(SOTECL)/%.cpp, $(BIDE)/%, $(TESTCL_SOURCES))
TESTSV_SOURCES = $(wildcard $(SOTESV)/test_*.cpp)
TESTSV_OBJECTS = $(patsubst $(SOTESV)/%.cpp, $(OBTE)/%.o, $(TESTSV_SOURCES))
TESTSV_TARGETS = $(patsubst $(SOTESV)/%.cpp, $(BIDE)/%, $(TESTSV_SOURCES))
TESTSH_SOURCES = $(wildcard $(SOTESH)/test_*.cpp)
TESTSH_OBJECTS = $(patsubst $(SOTESH)/%.cpp, $(OBTE)/%.o, $(TESTSH_SOURCES))
TESTSH_TARGETS = $(patsubst $(SOTESH)/%.cpp, $(BIDE)/%, $(TESTSH_SOURCES))

#
# targets
#

.PHONY: all client server shared release client-release server-release shared-release test clean

all: $(CLIENT_TARGET) $(SERVER_TARGET)
test: $(TESTCL_TARGETS) $(TESTSV_TARGETS) $(TESTSH_TARGETS)
client: $(CLIENT_TARGET)
server: $(SERVER_TARGET)
shared: $(SHARED_OBJECTS)
release: $(CLIENT_TARGET_RELEASE) $(SERVER_TARGET_RELEASE)
client-release: $(CLIENT_TARGET_RELEASE)
server-release: $(SERVER_TARGET_RELEASE)
shared-release: $(SHARED_OBJECTS_RELEASE)

#
# build 
#
$(CLIENT_TARGET): $(CLIENT_OBJECTS) $(SHARED_OBJECTS) $(BIDE)
	$(CC) -o $@ $(CLIENT_OBJECTS) $(SHARED_OBJECTS) $(CLIENT_ONLY_LIBS) $(LIBS)
	cp -r $(CLIENT_LABEL)/$(DATA_DIR)/ $(BIDE)/$(DATA_DIR)/

$(OBCLDE)/%.o: $(SOCL)/%.cpp $(OBCLDE)
	$(CC) -o $@ -c $(CFLAGS) $(DEBUG_FLAG) $(HEADER_PATH) $< 
	
$(SERVER_TARGET): $(SERVER_OBJECTS) $(SHARED_OBJECTS) $(BIDE)
	$(CC) -o $@ $(SERVER_OBJECTS) $(SHARED_OBJECTS) $(HEADER_PATH) $(LIBS)

$(OBSVDE)/%.o: $(SOSV)/%.cpp $(OBSVDE)
	$(CC) -o $@ -c $(CFLAGS) $(DEBUG_FLAG) $(HEADER_PATH) $< 
	
$(OBSHDE)/%.o: $(SOSH)/%.cpp $(OBSHDE)
	$(CC) -o $@ -c $(CFLAGS) $(DEBUG_FLAG) $(HEADER_PATH) $<
	
$(CLIENT_TARGET_RELEASE): $(CLIENT_OBJECTS_RELEASE) $(SHARED_OBJECTS) $(BIRE)
	$(CC) -o $@ $(CLIENT_OBJECTS_RELEASE) $(SHARED_OBJECTS) $(CLIENT_ONLY_LIBS) $(LIBS)
	cp -r $(CLIENT_LABEL)/$(DATA_DIR)/ $(BIRE)/$(DATA_DIR)/

$(OBCLRE)/%.o: $(SOCL)/%.cpp $(OBCLRE)
	$(CC) -o $@ -c $(CFLAGS) $(HEADER_PATH) $<
	
$(SERVER_TARGET_RELEASE): $(SERVER_OBJECTS_RELEASE) $(SHARED_OBJECTS) $(BIRE)
	$(CC) -o $@ $(SERVER_OBJECTS_RELEASE) $(SHARED_OBJECTS) $(LIBS)

$(OBSVRE)/%.o: $(SOSV)/%.cpp $(OBSVRE)
	$(CC) -o $@ -c $(CFLAGS) $(HEADER_PATH) $<
	
$(OBSHRE)/%.o: $(SOSH)/%.cpp $(OBSHRE)
	$(CC) -o $@ -c $(CFLAGS) $(HEADER_PATH) $<

$(BIDE)/test_%: $(OBTE)/test_client_%.o $(CLIENT_OBJECTS) $(SHARED_OBJECTS) $(BIRE)
	$(CC) -o $@ $< $(filter-out client/Debug/client.o, $(CLIENT_OBJECTS)) $(SHARED_OBJECTS) $(CLIENT_ONLY_LIBS) $(LIBS)

$(BIDE)/test_%: $(OBTE)/test_server_%.o $(SERVER_OBJECTS) $(SHARED_OBJECTS) $(BIRE)
	$(CC) -o $@ $< $(filter-out server/Debug/server.o, $(SERVER_OBJECTS)) $(SHARED_OBJECTS) $(LIBS)

$(BIDE)/test_%: $(OBTE)/test_shared_%.o $(SHARED_OBJECTS) $(BIRE)
	$(CC) -o $@ $< $(SHARED_OBJECTS) $(LIBS)

$(OBTE)/test_client_%.o: $(SOTECL)/test_%.cpp $(OBTE)
	$(CC) -o $@ -c $< $(CFLAGS) $(DEBUG_FLAG) $(HEADER_PATH) -Iclient
	
$(OBTE)/test_server_%.o: $(SOTESV)/test_%.cpp $(OBTE)
	$(CC) -o $@ -c $< $(CFLAGS) $(DEBUG_FLAG) $(HEADER_PATH) -Iserver

$(OBTE)/test_shared_%.o: $(SOTESH)/test_%.cpp $(OBTE)
	$(CC) -o $@ -c $< $(CFLAGS) $(DEBUG_FLAG) $(HEADER_PATH)
#
# mkdir
#
$(BIDE) $(BIRE) $(OBCLDE) $(OBCLRE) $(OBSVDE) $(OBSVRE) $(OBSHDE) $(OBSHRE) $(OBTE):
	@mkdir $@

clean:
	rm -rf Debug
	rm -rf Release
	rm -rf client/Debug
	rm -rf client/Release
	rm -rf server/Debug
	rm -rf server/Release
	rm -rf shared/Debug
	rm -rf shared/Release
	rm -rf test/Debug

	
