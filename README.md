# README #

This is a multiplayer game project.
Currently working on Client-Server communication.

### How to get started ###

Compiling
```bash
git clone https://[username]@bitbucket.org/recentnewcomer/projmp.git
make all
```

Run Server
```bash
./Debug/server 43323
```

Run Client
```bash
cd Debug
./client localhost 43323
```

### Naming convention guidelines ###
* We use the reduced version of [Hungarian notation](https://en.wikipedia.org/wiki/Hungarian_notation)
```c++
Prefixes:
    pPointer                        // "p" prefix for pointers
    aArray                          // "a" prefix for array of any known type
    class CSomeClass {              // "C" prefix for classes
        void m_MemberVariable;      // "m_" prefix for all member
        void Method();
    }             

Naming:
    typedef enum {
        ET_SOMEENUM,                // Capitalized initial of the type name as enum prefix. ("ET_" as "EnumType")
    } EnumType;
    typedef struct {} StructType;
    void some_function {};          // C-Style static function in the general namespace.
    int local_variable;


```